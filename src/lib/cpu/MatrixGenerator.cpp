#include "MatrixGenerator.h"

#include <omp.h>
#include <iostream>
#include <stdio.h>
#include <parallel/algorithm>

#define FAST_ROUND(x) x < 0 ? (int)(x - 0.5): (int)(x + 0.5)

#define Vector3D Eigen::Matrix<SOURCE_PRECISION, 1, 3>

/**
 * Convert 2D array to eigen compatible matrix
 */

EIGEN_MATRIX * convertMatrix(SOURCE_PRECISION **table, const int rows, const int cols){
    EIGEN_MATRIX *temp = new EIGEN_MATRIX(rows, cols);

	for(int x = 0; x < cols; x++){
        for(int y = 0; y < rows; y++){
            (*temp)(y, x) = table[y][x];
        }
    }

    return temp;
}

double timeDifferenceMS(struct timespec &start, struct timespec &end){
    timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }

    return temp.tv_sec * 1000 + temp.tv_nsec / 1000000.;
}

/**
 * Sorter comparison function used to sort sparse matrix values by col first
 */
bool sorter(const result &a, const result&b){
    if(a.y == b.y){
        return a.x < b.x;
    }

    return a.y < b.y;
}

MatrixGenerator::MatrixGenerator(){
	pthreads = 1;
    sortAMatrix = false;
}

void MatrixGenerator::setBeamletPointsInPatientCoords(SOURCE_PRECISION **table, const int rows, const int cols){
    beamletPointsInPatientCoords = convertMatrix(table, rows, cols);
}

void MatrixGenerator::setSourcePointsInPatientCoords(SOURCE_PRECISION **table, const int rows, const int cols){
    sourcePositionsInPatientCoords = convertMatrix(table, rows, cols);
}

void MatrixGenerator::setPerpendicularPlaneVectors(SOURCE_PRECISION **table, const int rows, const int cols){
    perpPlaneVectorsInPatientCoords = convertMatrix(table, rows, cols);
}

void MatrixGenerator::setKernel(SOURCE_PRECISION **table, const int rows, const int cols){
    kernel = convertMatrix(table, rows, cols);
}

void MatrixGenerator::setLabelmap(SOURCE_PRECISION *pOffset, SOURCE_PRECISION *pPixelDimensions, int* pLabelMapDimensions, int *pLabelMap){
	labelmapOffset = pOffset;
	labelmapPixelDimensions = pPixelDimensions;
	labelMap = pLabelMap;
	labelMapDimensions = pLabelMapDimensions;
}

void MatrixGenerator::setThreads(const int threads){
	pthreads = threads;
}

void MatrixGenerator::setSortAMatrix(bool sort){
    sortAMatrix = sort;
}

int MatrixGenerator::getRows(){
    return m_rows;
}

int MatrixGenerator::getCols(){
    return m_cols;
}

inline int MatrixGenerator::getLabelMapCoordinates(const int x, const int y, const int z){
    return x  + y * labelMapDimensions[1] + z * labelMapDimensions[0] * labelMapDimensions[1];
}

bool MatrixGenerator::findIntersection(Eigen::Matrix<SOURCE_PRECISION, 1, 3> &intersection, const Eigen::Matrix<SOURCE_PRECISION, 1, 3> &start, const Eigen::Matrix<SOURCE_PRECISION, 1, 3> &direction){
    const int x1 = round(start(0));
    const int y1 = round(start(1));
    const int z1 = round(start(2));

    const int x2 = round(start(0) + direction(0));
    const int y2 = round(start(1) + direction(1));
    const int z2 = round(start(2) + direction(2));

    const int dx = x2 - x1;
    const int dy = y2 - y1;
    const int dz = z2 - z1;

    const int ax = abs(dx) * 2;
    const int ay = abs(dy) * 2;
    const int az = abs(dz) * 2;

    const int sx = (dx < 0) ? -1 : 1;
    const int sy = (dy < 0) ? -1 : 1;
    const int sz = (dz < 0) ? -1 : 1;

    const SOURCE_PRECISION offsetX = labelmapOffset[0];
    const SOURCE_PRECISION offsetY = labelmapOffset[1];
    const SOURCE_PRECISION offsetZ = labelmapOffset[2];
    const SOURCE_PRECISION pixelDimensionX = labelmapPixelDimensions[0];
    const SOURCE_PRECISION pixelDimensionY = labelmapPixelDimensions[1];
    const SOURCE_PRECISION pixelDimensionZ = labelmapPixelDimensions[2];
    const int dimensionX = labelMapDimensions[0];
    const int dimensionY = labelMapDimensions[1];
    const int dimensionZ = labelMapDimensions[2];

    int x = x1;
    int y = y1;
    int z = z1;

    int idx = 1;

    //Those are the coordinates translated to the labelmap coordiantes
    int vX = round((x - offsetX) / pixelDimensionX);
    int vY = round((y - offsetY) / pixelDimensionY);
    int vZ = round((z - offsetZ) / pixelDimensionZ);

    if(ax > std::max(ay, az)){ //x dominant
        int yd = ay - ax / 2;
        int zd = az - ax / 2;

        while(true){

            if(vX >= 1 && vY >= 1 && vZ >= 1 && vX <= dimensionX && vY <= dimensionY && vZ <= dimensionZ){
                const int labelIndex = getLabelMapCoordinates(vX - 1, vY - 1, vZ - 1);

                if(labelMap[labelIndex] > 0){
                    intersection(0) = x;
                    intersection(1) = y;
                    intersection(2) = z;
                    return true;
                }
            }

            idx = idx + 1;

            if(x == x2){
                break;
            }

            if(yd >= 0){
                y += sy;
                vY = round((y - offsetY) / pixelDimensionY);
                yd -= ax;
            }

            if(zd >= 0){
                z += sz;
                vZ = round((z - offsetZ) / pixelDimensionZ);
                zd -= ax;
            }

            x += sx;
            vX = round((x - offsetX) / pixelDimensionX);
            yd += ay;
            zd += az;
        }
    }else if(ay >= std::max(ax, az)){ //y dominant
        int xd = ax - ay / 2;
        int zd = az - ay / 2;

        while(true){
            if(vX >= 1 && vY >= 1 && vZ >= 1 && vX <= dimensionX && vY <= dimensionY && vZ <= dimensionZ){
                const int labelIndex = getLabelMapCoordinates(vX - 1, vY - 1, vZ - 1);

                if(labelMap[labelIndex] > 0){
                    intersection(0) = x;
                    intersection(1) = y;
                    intersection(2) = z;
                    return true;
                }
            }

            if(y == y2){
                break;
            }

            idx += 1;
            if(xd >= 0){
                x += sx;
                vX = round((x - offsetX) / pixelDimensionX);
                xd -= ay;
            }

            if(zd >= 0){
                z += sz;
                vZ = round((z - offsetZ) / pixelDimensionZ);
                zd -= ay;
            }

            y += sy;
            vY = round((y - offsetY) / pixelDimensionY);
            xd += ax;
            zd += az;

        }

    }else if(az >= std::max(ax, ay)){ //z dominant
        int xd = ax - az / 2;
        int yd = ay - az / 2;

        while(true){
            if(vX >= 1 && vY >= 1 && vZ >= 1 && vX <= dimensionX && vY <= dimensionY && vZ <= dimensionZ){
                const int labelIndex = getLabelMapCoordinates(vX - 1, vY - 1, vZ - 1);

                if(labelMap[labelIndex] > 0){
                    intersection(0) = x;
                    intersection(1) = y;
                    intersection(2) = z;
                    return true;
                }
            }

            idx += 1;

            if(z == z2){
                break;
            }

            if(xd >= 0){
                x += sx;
                vX = round((x - offsetX) / pixelDimensionX);
                xd -= az;
            }

            if(yd >= 0){
                y += sy;
                vY = round((y - offsetY) / pixelDimensionY);
                yd -= az;
            }

            z += sz;
            vZ = round((z - offsetZ) / pixelDimensionZ);
            xd += ax;
            yd += ay;
        }
    }

    if(vX >= 1 && vY >= 1 && vZ >= 1 && vX <= dimensionX && vY <= dimensionY && vZ <= dimensionZ){
        const int labelIndex = getLabelMapCoordinates(vX - 1, vY - 1, vZ - 1);

        if(labelMap[labelIndex] > 0){
            intersection(0) = x;
            intersection(1) = y;
            intersection(2) = z;
            return true;
        }
    }

    return false;
}

int MatrixGenerator::kernelMaxColSquared(){
    int radLengthMax = kernel->cols();
    for(int i = 0; i < kernel->cols(); i++){
        if(kernel->col(i).maxCoeff() <= MIN_KERNEL_VAL){
            radLengthMax = i;
            break;
        }
    }

    radLengthMax++;
    radLengthMax *= radLengthMax;

    return radLengthMax;
}

EIGEN_MATRIX* MatrixGenerator::generateRealCoordinates(const int threshold){
    std::vector< Eigen::Matrix<SOURCE_PRECISION, 1, 3> > coordinates;

    printf("generateRealCoordinates %d %d %d\n", labelMapDimensions[0], labelMapDimensions[1],labelMapDimensions[2]);

    //Construct the real coordinates
    for(int z = 0; z < labelMapDimensions[2]; z++){
        for(int y = 0; y < labelMapDimensions[1]; y++){
            for(int x = 0; x < labelMapDimensions[0]; x++){
                const int index = getLabelMapCoordinates(x, y, z );
                if(labelMap[index] > threshold){
                    Vector3D point;
                    point(0) = (x + 1) * labelmapPixelDimensions[0] + labelmapOffset[0];
                    point(1) = (y + 1) * labelmapPixelDimensions[1] + labelmapOffset[1];
                    point(2) = (z + 1) * labelmapPixelDimensions[2] + labelmapOffset[2];

                    coordinates.push_back(point);
                }
            }
        }
    }

    EIGEN_MATRIX *gridinRealCoords = new EIGEN_MATRIX(coordinates.size(), 3);
    for(int i = 0; i < coordinates.size(); i++){
        gridinRealCoords->row(i) = coordinates[i];
    }

    return gridinRealCoords;
}

std::vector<result> * MatrixGenerator::generateMatrix(){
	omp_set_num_threads(pthreads);

    struct timespec startAll;
    struct timespec end;

    clock_gettime(CLOCK_MONOTONIC, &startAll);

    const int nbrOfBeamlets = beamletPointsInPatientCoords->rows();
    const int radLengthMax = kernelMaxColSquared();

    const EIGEN_MATRIX* gridinRealCoords = generateRealCoordinates(1);
    
    const int kernelRowMax = kernel->rows() - 1;
    const int kernelColsMax = kernel->cols() - 1;
    const int rows = gridinRealCoords->rows();

    m_rows = rows;
    m_cols = nbrOfBeamlets;
	
    std::vector<result> * results = new std::vector<result>;
	
    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("Generate A preparatory work: %f ms \n", timeDifferenceMS(startAll, end) );
	
    clock_gettime(CLOCK_MONOTONIC, &startAll);
	
    std::cout << "Test " << nbrOfBeamlets << " beamlets" << std::endl;

    std::vector<result> tempResults;

    #pragma omp parallel for private(tempResults)
    for(int beamlet = 0; beamlet < nbrOfBeamlets; beamlet++){
        const Vector3D currentSource = sourcePositionsInPatientCoords->row(beamlet);

        const Vector3D rc = beamletPointsInPatientCoords->row(beamlet) - currentSource;

        const Vector3D planej = perpPlaneVectorsInPatientCoords->block(beamlet, 0 , 1, 3);

        const Vector3D planei = perpPlaneVectorsInPatientCoords->block(beamlet, 3 , 1, 3);

        //Temp stuff
        Vector3D intersectionRealCoords;

        findIntersection(intersectionRealCoords, currentSource, rc);
        const double SSD = (currentSource - intersectionRealCoords).norm();
        const double tempRCNormSquare = rc.squaredNorm();
        const Vector3D dividedRC = rc / tempRCNormSquare;

        tempResults.clear();

        for(int i = 0; i < rows; i++){ //OK
            const Vector3D superR = gridinRealCoords->row(i) - currentSource; //22%, callgrind
            const Vector3D rrcProjection = dividedRC * superR.cwiseProduct(rc).sum(); //24%, callgrind
            const Vector3D tempVar = superR - rrcProjection; //17%, callgrind

            //This rounding trick (+0.5) should be ok, as we work with positive values and it does not matter for negative ones
            const double dj = (planej * ( tempVar.cwiseProduct(planej)).sum() ).squaredNorm(); //24%, callgrind
            if(dj >= radLengthMax){//fast abort if we are sure that radlength will fall on a column that is too small anyway
                continue;
            }

            const double di = (planei * (tempVar.cwiseProduct(planei)).sum()).squaredNorm();

            const int tempRad = (int)(sqrt(dj + di) + 0.5);
            const int radLength = std::min(tempRad, kernelColsMax);

            const int tempDose = (int)(rrcProjection.norm() - SSD + 0.5);
            const int doseDepth = std::min(std::max(0, tempDose), kernelRowMax);

            const SOURCE_PRECISION matrixvals = (*kernel)(doseDepth, radLength);

            if(matrixvals > MIN_KERNEL_VAL){
                result value;
                value.x = beamlet;
                value.y = i;
                value.value = matrixvals;
                tempResults.push_back(value);
            }
        }

        #pragma omp critical
        {
            results->insert(results->end(), tempResults.begin(), tempResults.end());
        }
    }
    
    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("time used to generate A %f ms \n", timeDifferenceMS(startAll, end) );
	
    delete gridinRealCoords;

    if(sortAMatrix){
        clock_gettime(CLOCK_MONOTONIC, &startAll);
        __gnu_parallel::sort(results->begin(), results->end(), sorter);
        clock_gettime(CLOCK_MONOTONIC, &end);
        printf("Sorted A matrix in %f ms \n", timeDifferenceMS(startAll, end) );
    }

	return results;
}


//TODO: Handle the insane code duplication
SOLUTION_PRECISION * MatrixGenerator::generateDosemap(OPTIMIZATION_PRECISION *xSol, const int xSolLength){
    omp_set_num_threads(pthreads);

    const int nbrOfBeamlets = beamletPointsInPatientCoords->rows();
    const int radLengthMax = kernelMaxColSquared();
    const EIGEN_MATRIX* gridinRealCoords = generateRealCoordinates(0);

    const int kernelRowMax = kernel->rows() - 1;
    const int kernelColsMax = kernel->cols() - 1;
    const int rows = gridinRealCoords->rows();

    struct timespec startAll;
    struct timespec end;

    clock_gettime(CLOCK_MONOTONIC, &startAll);

    const int resultSize = labelMapDimensions[0] * labelMapDimensions[1] * labelMapDimensions[2];
    SOLUTION_PRECISION *result = new SOLUTION_PRECISION[resultSize];
    memset(result, 0, sizeof(SOLUTION_PRECISION)* resultSize);

    #pragma omp parallel for
    for(int beamlet = 0; beamlet < nbrOfBeamlets; beamlet++){
        const OPTIMIZATION_PRECISION beamletSol = xSol[beamlet];
        if(beamletSol <= 0){ //Ignore beamlets that dont actually do anything
            continue;
        }

        const Vector3D currentSource = sourcePositionsInPatientCoords->row(beamlet);

        const Vector3D rc = beamletPointsInPatientCoords->row(beamlet) - currentSource;

        const Vector3D planej = perpPlaneVectorsInPatientCoords->block(beamlet, 0 , 1, 3);
        const Vector3D planei = perpPlaneVectorsInPatientCoords->block(beamlet, 3 , 1, 3);

        //Temp stuff
        Vector3D intersectionRealCoords;

        findIntersection(intersectionRealCoords, currentSource, rc);
        const double SSD = (currentSource - intersectionRealCoords).norm();
        const double tempRCNormSquare = rc.squaredNorm();
        const Vector3D dividedRC = rc / tempRCNormSquare;

        for(int i = 0; i < rows; i++){
            const Vector3D superR = gridinRealCoords->row(i) - currentSource;
            const Vector3D rrcProjection = dividedRC * superR.cwiseProduct(rc).sum();
            const Vector3D tempVar = superR - rrcProjection;

            const double dj = (planej * (tempVar.cwiseProduct(planej)).sum()).squaredNorm();
            if(dj >= radLengthMax){//fast abort if we are sure that radlength will fall on a column that is too small anyway
                continue;
            }

            const double doseDepthRaw = rrcProjection.norm() - SSD;
            if(doseDepthRaw < 0){
                continue;
            }

            const double di = (planei * (tempVar.cwiseProduct(planei)).sum()).squaredNorm();

            int temp = (int)(sqrt(dj + di) + 0.5);;//(int)round(sqrt(dj + di));

            const int radLength = std::min(temp, kernelColsMax); //temp can not be < 0

            const int doseDepth = std::min((int)(doseDepthRaw + 0.5), kernelRowMax);

            const SOURCE_PRECISION matrixvals = (*kernel)(doseDepth, radLength);

            if(matrixvals > MIN_KERNEL_VAL){ //TODO: write this in a nice manner
                const Vector3D pos = gridinRealCoords->row(i);
                const int x = ((pos(0) - labelmapOffset[0]) / labelmapPixelDimensions[0]) - 1;
                const int y = ((pos(1) - labelmapOffset[1]) / labelmapPixelDimensions[1]) - 1;
                const int z = ((pos(2) - labelmapOffset[2]) / labelmapPixelDimensions[2]) - 1;

                const int index = getLabelMapCoordinates(x, y, z );

                const SOURCE_PRECISION doseAddition = beamletSol * matrixvals;

                #pragma omp atomic
                result[index] += doseAddition;

                /*#pragma omp critical result_addition
                {
                     result[index] += (beamletSol * matrixvals);
                }*/
            }
        }
    }

    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("time used to generate dosemap %f ms\n",timeDifferenceMS(startAll, end));

    return result;
}
