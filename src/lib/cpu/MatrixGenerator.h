 
#ifndef LIB_BIGMAT_MATRIX_GENERATOR_H
#define LIB_BIGMAT_MATRIX_GENERATOR_H

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <vector>
#include "Optimizer.h"

#define MIN_KERNEL_VAL 1e-2

#define EIGEN_MATRIX Eigen::Matrix<SOURCE_PRECISION, -1, -1, Eigen::RowMajor>

/**
 * This struct represents one value of the A matrix.
 * It contains its coordinates and the value associated
 */
struct result {
    int x;
    int y;
    OPTIMIZATION_PRECISION value;
};

/**
* Debug function used for profiling
**/
double timeDifferenceMS(struct timespec &start, struct timespec &end);
 
class MatrixGenerator {
public:

	MatrixGenerator();
	
    void setBeamletPointsInPatientCoords(SOURCE_PRECISION **table, const int rows, const int cols);
	
    void setSourcePointsInPatientCoords(SOURCE_PRECISION **table, const int rows, const int cols);

    void setPerpendicularPlaneVectors(SOURCE_PRECISION **table, const int rows, const int cols);
	
    /**
     * Set the kernel (beamlet doses) to be used for the A matrix and the dosemap
     */
    void setKernel(SOURCE_PRECISION **table, const int rows, const int cols);
	
    /**
     * Set the labelmap to be used for (patient/optimization specific)
     */
    void setLabelmap(SOURCE_PRECISION *pOffset, SOURCE_PRECISION *pPixelDimensions, int* pLabelMapDimensions, int *pLabelMap);

    /**
     * If set to true, the matrix created by generateMatrx() will be sorted by row/col.
     * Defaults to false
     */
    void setSortAMatrix(bool sort);
	
    /**
     * Generate the A matrix used for the optimization
     */
    std::vector<result> * generateMatrix();
	
    /**
     * Generate the dosemap based on a particular solution.
     */
    SOLUTION_PRECISION * generateDosemap(OPTIMIZATION_PRECISION *xSol, const int xSolLength);

	void setThreads(const int threads);

    /**
     * Return the number of rows of the A matrix that has been generated
     */
    int getRows();

    /**
     * Return the number of columns of the A matrix that has been generated
     */
    int getCols();
	
private:
	
	inline int getLabelMapCoordinates(const int x, const int y, const int z);
    bool findIntersection(Eigen::Matrix<SOURCE_PRECISION, 1, 3> &intersection, const Eigen::Matrix<SOURCE_PRECISION, 1, 3> &start, const Eigen::Matrix<SOURCE_PRECISION, 1, 3> &direction);

    /**
     * Returns the squared index of the first column in the kernel that has no value > than the defined MIN_KERNEL_VAL
     */
    int kernelMaxColSquared();

    /**
     * returns the coordinates of all interesting voxels (labelmap > 1) in "real" coordinates
     */
    EIGEN_MATRIX* generateRealCoordinates(const int threshold);

    EIGEN_MATRIX * beamletPointsInPatientCoords;
    EIGEN_MATRIX * perpPlaneVectorsInPatientCoords;
    EIGEN_MATRIX * sourcePositionsInPatientCoords;
    EIGEN_MATRIX * kernel;
	
    SOURCE_PRECISION *labelmapOffset;
    SOURCE_PRECISION *labelmapPixelDimensions;
	int* labelMap;
	int* labelMapDimensions;
	
	int pthreads;
	
    int m_rows;
    int m_cols;

    bool sortAMatrix;
};

#endif
