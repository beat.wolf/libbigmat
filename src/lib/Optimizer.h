#ifndef LIB_BIGMAT_OPTIMIZER_H
#define LIB_BIGMAT_OPTIMIZER_H

#include <string>

#define SOURCE_PRECISION double
#define OPTIMIZATION_PRECISION float
#define SOLUTION_PRECISION double

/**
 * This class holds all parameters needed to generate the A matrix
 **/
class AMatrixParameters {
	
public:

    void setBeamletPointsInPatientCoords(SOURCE_PRECISION **table, int rows, int cols)
	{
		beamletPointsInPatientCoords = table;
		beamletPointsInPatientCoordsDims[0] = rows;
		beamletPointsInPatientCoordsDims[1] = cols;
	}
	
    void setSourcePointsInPatientCoords(SOURCE_PRECISION **table, int rows, int cols)
	{
		sourcePointsInPatientCoords = table;
		sourcePointsInPatientCoordsDims[0] = rows;
		sourcePointsInPatientCoordsDims[1] = cols;
	}

    void setPerpendicularPlaneVectors(SOURCE_PRECISION **table, int rows, int cols)
	{
		perpendicularPlaneVectors = table;
		perpendicularPlaneVectorsDims[0] = rows;
		perpendicularPlaneVectorsDims[1] = cols;
	}
	
    SOURCE_PRECISION** getBeamletPointsInPatientCoords()
	{
		return beamletPointsInPatientCoords;
	}
	
    int * getBeamletPointsInPatientCoordsDims()
	{
        return beamletPointsInPatientCoordsDims;
	}
	
    SOURCE_PRECISION** getSourcePointsInPatientCoords()
	{
        return sourcePointsInPatientCoords;
	}
	
    int *getSourcePointsInPatientCoordsDims()
	{
        return sourcePointsInPatientCoordsDims;
	}
	
    SOURCE_PRECISION** getPerpendicularPlaneVectors()
	{
		return perpendicularPlaneVectors;
	}
	
    int * getPerpendicularPlaneVectorsDims()
	{
        return perpendicularPlaneVectorsDims;
	}

    void setLabelmap(SOURCE_PRECISION *pOffset, SOURCE_PRECISION *pPixelDimensions, int *pLabelMapDimensions, int *pLabelMap){
		labelmapOffset = pOffset;
		labelmapPixelDimensions = pPixelDimensions;
        labelMapDimensions = pLabelMapDimensions;
        labelMap = pLabelMap;
    }

	int* getLabelmap()
	{
		return labelMap;
	}
	
    int *getLabelmapDimensions()
	{
        return labelMapDimensions;
	}
	
    SOURCE_PRECISION * getLabelmapPixelDimensions()
	{
        return labelmapPixelDimensions;
	}
	
    SOURCE_PRECISION * getLabelmapOffset()
	{
        return labelmapOffset;
	}

    void setKernel(SOURCE_PRECISION **table, int rows, int cols){
        kernel = table;
        kernelDimensions[0] = rows;
        kernelDimensions[1] = cols;
    }

    SOURCE_PRECISION **getKernel(){
        return kernel;
    }

    int *getKernelDimensions(){
        return kernelDimensions;
    }
	
	void readAMatrix(std::string &file){
		matrixAFileName = file;		
	}

    std::string getAMatrixFilename(){
        return matrixAFileName;
    }
	
private:
		
	std::string matrixAFileName;
	
    SOURCE_PRECISION **beamletPointsInPatientCoords;
	int beamletPointsInPatientCoordsDims[2];
    SOURCE_PRECISION **sourcePointsInPatientCoords;
	int sourcePointsInPatientCoordsDims[2];
    SOURCE_PRECISION **perpendicularPlaneVectors;
	int perpendicularPlaneVectorsDims[2];
	
	
    SOURCE_PRECISION *labelmapOffset;
    SOURCE_PRECISION *labelmapPixelDimensions;
	int *labelMap;
    int *labelMapDimensions;

    SOURCE_PRECISION **kernel;
    int kernelDimensions[2];
};

/**
 * This class contains all the parameters needed to launch the optimization problem
 */
class OptimizationParameters {
	
public:
	
    /**
     * Set the amount of possible beams, commonly angles, that have to be considered
     * @brief setBeamCount
     * @param count
     */
	void setBeamCount(int count){
		beamCount = count;
	}

    int getBeamCount(){
        return beamCount;
    }
	
    /**
     * Set width (by beamlets) for every beam
     * @brief setBeamWidth
     * @param width
     */
	void setBeamWidth(int width){
		beamletWidth = width;
	}

    int getBeamWidth(){
        return beamletWidth;
    }
	
    /**
     * Set height (by beamlets) for every beam
     * @brief setBeamHeight
     * @param height
     */
	void setBeamHeight(int height){
		beamletHeight = height;
	}
	
    int getBeamHeight(){
        return beamletHeight;
    }

    /**
     *
     * @brief setRegions
     * @param count
     * @param boundaries
     * @param ids
     */
	void setRegions(int count, float *upperBoundaries, float *lowerBoundaries, int *ids){
		regionTypes = count;
		regionUpperBounds = upperBoundaries;
		regionLowerBounds = lowerBoundaries;
		regionIDs = ids;
	}

    int getRegionTypes(){
        return regionTypes;
    }

    float * getRegionUpperBounds(){
        return regionUpperBounds;
    }
    
    float * getRegionLowerBounds(){
        return regionLowerBounds;
    }

    int * getRegionIDs(){
        return regionIDs;
    }

    //Stage 1

    void setRelativeErrorStage1(float error){
        relativeErrorStage1 = error;
	}

    float getRelativeErrorStage1(){
        return relativeErrorStage1;
    }
	
    void setMaxIterationsStage1(int max){
        maxIterationsStage1 = max;
	}

    int getMaxIterationsStage1(){
        return maxIterationsStage1;
    }
	
    void setGammaStage1(float value){
        gammaStage1 = value;
    }

    float getGammaStage1(){
        return gammaStage1;
    }

    //Stage 2

    void setRelativeErrorStage2(float error){
        relativeErrorStage2 = error;
    }

    float getRelativeErrorStage2(){
        return relativeErrorStage2;
    }

    void setMaxIterationsStage2(int max){
        maxIterationsStage2 = max;
    }

    int getMaxIterationsStage2(){
        return maxIterationsStage2;
    }

    void setGammaStage2(float value){
        gammaStage2 = value;
    }

    float getGammaStage2(){
        return gammaStage2;
    }

private:
	
	int beamCount;
	int beamletWidth;
	int beamletHeight;
	
	int regionTypes;
	
	float *regionUpperBounds; //d1 - d7
	float *regionLowerBounds; //Can be either -1 (ignored) or a positive value
	int *regionIDs; //2-8 (as1 = find(Y==2); %Transition region)
	
    float relativeErrorStage1;
	//options1.max_iter = 15000;
    float maxIterationsStage1;
	//options1.gamma = 1e5;
    float gammaStage1;

    float relativeErrorStage2;
    float maxIterationsStage2;
    float gammaStage2;
};

/**
 * BigMatOptimizer public interface. All optimizers implement this interface
 */
class Optimizer {
	
public:

    virtual ~Optimizer() {}

	/**
	* Set all parameters needed to create the matrix A
	*/
    virtual void setMatrixAParameters(AMatrixParameters &parameters) = 0;
	
	/**
	 * Generate the Matrix A to be used, based on the parameters previously set in setMatrixAParameters
	 */
    virtual int generateA() = 0;
	
    virtual void setOptimizationParameters(OptimizationParameters &parameters) = 0;
	
	/**
	 * Perform the problem optimization.
	 */
    virtual int optimize() = 0;

    /**
     * Generate the dosemap, based on the information used and created in the optimize() step
     */
    virtual void generateDosemap() = 0;

	/**
	 * Returns the size of the xsol vector
	 */
    virtual int getXSolSize() = 0;
	
	/**
	 * Returns the XSol vector
	 */
    virtual OPTIMIZATION_PRECISION * getXSol() = 0;

	/**
	 * Returns the size of the dosemap vector
	 */
    virtual int getDosemapSize() = 0;
	
	/**
	 * Returns the dosemap as a vector
	 */
    virtual SOLUTION_PRECISION * getDosemap() = 0;
};

#endif
