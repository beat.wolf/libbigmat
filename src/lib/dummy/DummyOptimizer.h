#ifndef LIB_BIGMAT_DUMMY_OPTIMIZER_H
#define LIB_BIGMAT_DUMMY_OPTIMIZER_H

#include "Optimizer.h"

/**
 * This is a dummy implementation of the Optimizer.
 * It returns an xSol with the size of:
 * OptimizationParameters.getBeamCount() * OptimizationParameters.getBeamWidth() * OptimizationParameters.getBeamHeight();
 *
 * The dummy implementation throws errors if the different functions are not called in the right order
 */
class DummyOptimizer : public Optimizer {
	
public :
	DummyOptimizer();
	
	~DummyOptimizer();
	
	void setMatrixAParameters(AMatrixParameters &parameters);
	
    int generateA();

	void setOptimizationParameters(OptimizationParameters &parameters);
	
    int optimize();

    void generateDosemap();

	int getXSolSize();
	
    OPTIMIZATION_PRECISION * getXSol();

    int getDosemapSize();
	
    SOLUTION_PRECISION * getDosemap();
	
private:
	AMatrixParameters aParamaters;
	OptimizationParameters oParameters;
	
	int xSolSize;
    OPTIMIZATION_PRECISION *xSol;

    bool matrixALoaded;

    int labelMapSize;
    int **labelMap;

    SOLUTION_PRECISION *dosemap;
};

#endif
