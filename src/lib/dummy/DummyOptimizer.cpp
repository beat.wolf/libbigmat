#include "DummyOptimizer.h"

#include <iostream>
#include <cstdlib>
#include <assert.h>
#include "cpu/MatrixGenerator.h"

#define THREADS 8

DummyOptimizer::DummyOptimizer() : xSolSize(0), xSol(0), matrixALoaded(false), labelMapSize(0), labelMap(0){
    std::cout << "Create dummy" << std::endl;
}
	
DummyOptimizer::~DummyOptimizer(){

    if(xSol){
        std::cout << "delete xSol" << std::endl;
        delete [] xSol;
    }
}

void DummyOptimizer::setMatrixAParameters(AMatrixParameters &parameters){	
	aParamaters = parameters;
}

int DummyOptimizer::generateA(){

    MatrixGenerator generator;
    generator.setBeamletPointsInPatientCoords(aParamaters.getBeamletPointsInPatientCoords(),
                                              aParamaters.getBeamletPointsInPatientCoordsDims()[0],
                                              aParamaters.getBeamletPointsInPatientCoordsDims()[1]);

    generator.setPerpendicularPlaneVectors(aParamaters.getPerpendicularPlaneVectors(),
                                           aParamaters.getPerpendicularPlaneVectorsDims()[0],
                                            aParamaters.getPerpendicularPlaneVectorsDims()[1]);

    generator.setSourcePointsInPatientCoords(aParamaters.getSourcePointsInPatientCoords(),
                                           aParamaters.getSourcePointsInPatientCoordsDims()[0],
                                            aParamaters.getSourcePointsInPatientCoordsDims()[1]);

    generator.setKernel(aParamaters.getKernel(), aParamaters.getKernelDimensions()[0], aParamaters.getKernelDimensions()[1]);

    generator.setLabelmap(aParamaters.getLabelmapOffset(), aParamaters.getLabelmapPixelDimensions(), aParamaters.getLabelmapDimensions(), aParamaters.getLabelmap());

    generator.setThreads(THREADS);

    generator.setSortAMatrix(true);

    std::vector<result> *result = generator.generateMatrix();

    std::cout << "Found " << result->size() << " nnz" << std::endl;

    int matlab = 33060258;

    if(result->size() != matlab){
        std::cout << "NNZ was wrong, should have been " << matlab << std::endl;
    }

    delete result;

    return 0;
}

void DummyOptimizer::setOptimizationParameters(OptimizationParameters &parameters){
	oParameters = parameters;
}

int DummyOptimizer::optimize(){

    xSolSize = aParamaters.getBeamletPointsInPatientCoordsDims()[0];

    if(xSol){
        delete [] xSol;
    }
	
    xSol = new OPTIMIZATION_PRECISION[xSolSize];
	
    srand (time(NULL));
	for(int i = 0;i < xSolSize; i++){
		xSol[i] = (rand()/(double)(RAND_MAX));
	}

    return 0;
}

void DummyOptimizer::generateDosemap(){
    MatrixGenerator generator;
    generator.setBeamletPointsInPatientCoords(aParamaters.getBeamletPointsInPatientCoords(),
                                              aParamaters.getBeamletPointsInPatientCoordsDims()[0],
                                              aParamaters.getBeamletPointsInPatientCoordsDims()[1]);

    generator.setPerpendicularPlaneVectors(aParamaters.getPerpendicularPlaneVectors(),
                                           aParamaters.getPerpendicularPlaneVectorsDims()[0],
                                            aParamaters.getPerpendicularPlaneVectorsDims()[1]);

    generator.setSourcePointsInPatientCoords(aParamaters.getSourcePointsInPatientCoords(),
                                           aParamaters.getSourcePointsInPatientCoordsDims()[0],
                                            aParamaters.getSourcePointsInPatientCoordsDims()[1]);

    generator.setKernel(aParamaters.getKernel(), aParamaters.getKernelDimensions()[0], aParamaters.getKernelDimensions()[1]);

    generator.setLabelmap(aParamaters.getLabelmapOffset(), aParamaters.getLabelmapPixelDimensions(), aParamaters.getLabelmapDimensions(), aParamaters.getLabelmap());

    generator.setThreads(THREADS);

    dosemap = generator.generateDosemap(xSol, xSolSize);

}

int DummyOptimizer::getXSolSize(){
	return xSolSize;
}

OPTIMIZATION_PRECISION * DummyOptimizer::getXSol(){
	return xSol;
}

int DummyOptimizer::getDosemapSize(){
    return aParamaters.getLabelmapDimensions()[0] * aParamaters.getLabelmapDimensions()[1] * aParamaters.getLabelmapDimensions()[2];
}

SOLUTION_PRECISION * DummyOptimizer::getDosemap(){
    return dosemap;
}
