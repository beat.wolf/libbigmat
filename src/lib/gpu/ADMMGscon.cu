/*
 *  ADMMGscon.cu
 *
 *  Created on: Jan 4, 2015
 *  Author: Marco Lourenço, Beat Wolf
 *  Company: HEIA-FR
 */

#include <cstring>
#include <algorithm>
#include <cstdio>
#include <iostream>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>

#include "time_utils.h"
#include "ADMMGscon.h"
#include "cuda_utils.h"
#include "cpu/MatrixGenerator.h"

/*
 * y and Y (yLabelMap) must have same size !
 */
__global__ void findAndReplaceInVector(float* vector, int* yLabelMap,
		int findValue, float replaceByValue, int vectorSize, int* indexVector) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < vectorSize && yLabelMap[tid] == findValue) {
		vector[tid] = replaceByValue;
		indexVector[tid] = 1;
	}
}


/*
 * Used by optimize() when resizing matrix B into small B, it copies some columns and values from B to small B
 */
__global__ void asyncSplitKernel(int * destCsrCol, int * sourceCsrCol, float * destVal, float * sourceVal,
		int nbOfEltsToCopy, int offset){
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if(tid < nbOfEltsToCopy){//This might not be required if kernel is correctly initialized
		destCsrCol[tid] = sourceCsrCol[tid] - offset;
		destVal[tid] = sourceVal[tid];
	}
}

/*
 * Instead of creating w, W and doing B=W*A, just directly flip signs inside A.
 *
 * To do that we need index of Y where Y == 3. The lines in A corresponding to those index must flip sign.
 */
__global__ void flipSignsMatrixACSR(int* yLabelMap, int vectorSize, int findValue,
		int* csrRowDevicePtr, float* csrValDevicePtr) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < vectorSize && yLabelMap[tid] == findValue) {
		/*
		 * tid is the line of the matrix A where values must flip sign
		 * translate that ID in CSR format
		 */
		int lineStartIndex = csrRowDevicePtr[tid]; // elts of line #tid start at index lineCSRID
		int lineStopIndex = vectorSize; // if stop is the end
		if (tid + 1 != vectorSize)
			lineStopIndex = csrRowDevicePtr[tid + 1]; // elts of line #tid stop at index lineCSRID
		for (int i = lineStartIndex; i < lineStopIndex; i++) {
			csrValDevicePtr[i] = -csrValDevicePtr[i]; // flip sign
		}
	}
}

/*
 * Remove a fixed value to all values in array
 */
__global__ void adjustNewRowArray(int* csrRowDevicePtr,int size, int valueToRemove)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < size) {
		csrRowDevicePtr[tid] -=valueToRemove;
	}
}

/*
 * Flip sign of all values in array
 */
__global__ void flipSignVector(float* csrDevicePtr,int size)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < size) {
		csrDevicePtr[tid] *= -1.0f;
	}
}

/*
 * Init all values of array to initValue
 */
__global__ void initArrayKernel(float* array, int size, float initValue)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < size)
	{
		array[tid] = initValue;
	}
}

/*
 * compute the L2 norm
 */
__global__ void l2NormVector(float* resultDevicePtr, float* vectorDevicePtr, int G, int NG)
{
	__shared__ float squareSums;
	int tid = threadIdx.x;
	int subVectorIndex = blockIdx.x;
	if(tid==0) squareSums = 0.0;
	if (tid < NG)
	{
		while(tid < G)
		{
			float crt = vectorDevicePtr[subVectorIndex*G+tid];
			atomicAdd(&squareSums,(crt*crt));
			tid+=NG;
		}
		__syncthreads();
		resultDevicePtr[subVectorIndex] = std::sqrt(squareSums);
	}

}

/*
 * New small B matrix will only have certain columns from original B matrix
 *
 * To know new nnz each block will take care of one of the index in normvecIndex and count the elements
 */
__global__ void resizeBCountNNZ(int* csrColDevicePtr, int* normvecIndexDevicePtr,
		int* resultDevicePtr, int NB, int nnz)
{
	__shared__ int nnzCount;
	/* starting and ending columns represent a group a columns in original B matrix
	 * that will be copied to new small B, so we will count all elements that belong
	 * to those columns to knew nnz
	 */
	const int blockID = blockIdx.x;
	const int startingColumn = normvecIndexDevicePtr[blockID]*NB;
	const int endingColumn = startingColumn+NB;
	int tid = threadIdx.x;
	const int NB_THREADS = blockDim.x;
	if(tid==0)
	{
		nnzCount = 0;
	}
	// each thread checks in csrCol for columns belonging to [start;end[
	while (tid < nnz) {
		if(csrColDevicePtr[tid]>=startingColumn && csrColDevicePtr[tid]<endingColumn)
		{
			atomicAdd(&nnzCount,1);
		}
		tid += NB_THREADS;
	}
	__syncthreads();
	resultDevicePtr[blockID] = nnzCount;
}

/*
 * Remove a fixed value to all values in array
 */
__global__ void removeValFromDeviceArray(int* array, int size, int value)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < size)
	{
		array[tid] -= value;
	}
}

/*
 * Hard thresholding
 */
__global__ void hardt(float* r, float* xsol, int vectorSize)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < vectorSize) {
		float op = r[tid];
		xsol[tid] = op;
		if (op < 0.0) {
			xsol[tid] = 0.0;
		}
	}
}

/*
 * Transform all negative values to zero
 */
__global__ void negativeToZeroVector(float* vector, long vectorSize) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < vectorSize && vector[tid] < 0.0) {
		vector[tid] = 0.0;
	}
}

/*
 * Perform a vector division
 * output = vec / abs(y)
 */
__global__ void vectorDivision(float* vec, float* y, float* output,
		float vectorSize) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < vectorSize) {
		output[tid] = vec[tid] / std::abs(y[tid]);
	}
}

/*
 * Soft thresholding
 */
__global__ void soft(float* r, int vectorSize, float* d, float* x) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < vectorSize) {
		float op = r[tid] - d[tid];
		x[tid] = op;
		if (op < 0.0) {
			x[tid] = 0.0;
		}
	}
}

/*
 * Transform all zero values to machine epsilon values
 */
__global__ void zeroToEpsilonVector(float* vector, long vectorSize, float epsilon) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < vectorSize && vector[tid] == 0.0) {
		vector[tid] = epsilon;
	}
}

/*
 * Globaly replaces matlab code sol = sol*W by not reshaping vector into matrix
 */
__global__ void l2l1_special_mult(float* xDevicePtr, float* TDevicePtr, float* solDevicePtr,
		int G, int NG) {
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid < (G*NG)) {
		int TIndex = tid/G;
		solDevicePtr[tid] = xDevicePtr[tid] * TDevicePtr[TIndex];
	}
}

/*
 * Comparators used by sorting algorithms
 */
float* globalNormvecHostPtr;
bool sortDescendComparator (int i,int j) { return (globalNormvecHostPtr[i]>globalNormvecHostPtr[j]); }
bool sortAscendComparator (int i,int j) { return (i<j); }
bool matrixAComparator (const result &i,const result &j)
{
	if(i.y == j.y)
	{
		return i.x < j.x;
	}
	return i.y < j.y;
}

ADMMGscon::ADMMGscon(const int inputNbGPUs)
{
	HANDLE_ERROR(cudaPeekAtLastError());
	NGPUS = inputNbGPUs;
	printf("Creating ADMMGscon object for %d GPUs\n",NGPUS);
	stream = new cudaStream_t[NGPUS];
	blasHandle = 0;
	blasStatus = CUBLAS_STATUS_SUCCESS;
	status = CUSPARSE_STATUS_SUCCESS;
	handle = 0;
	descr = 0;
	initZ = true;
	mu = 0;
	nnl2l1_prox_maxiter = 1;
	init();
}

ADMMGscon::~ADMMGscon() {
	HANDLE_ERROR(cudaPeekAtLastError());
	if(xsolHostFinal)
		free(xsolHostFinal);
}

void ADMMGscon::setMatrixAParameters(AMatrixParameters &parameters)
{
	this->matrixParameters = parameters;
}

/*
 * Generates the dosemap, should happen after optimize()
 */
void ADMMGscon::generateDosemap()
{
	printf("\n\n\n\n--------------------------------------------------------\n");
	printf("Generating the dosemap\n");
	printf("--------------------------------------------------------\n\n\n\n");
	tic();

	MatrixGenerator generator;
	generator.setBeamletPointsInPatientCoords(matrixParameters.getBeamletPointsInPatientCoords(),
			matrixParameters.getBeamletPointsInPatientCoordsDims()[0],
			matrixParameters.getBeamletPointsInPatientCoordsDims()[1]);

	generator.setPerpendicularPlaneVectors(matrixParameters.getPerpendicularPlaneVectors(),
			matrixParameters.getPerpendicularPlaneVectorsDims()[0],
			matrixParameters.getPerpendicularPlaneVectorsDims()[1]);

	generator.setSourcePointsInPatientCoords(matrixParameters.getSourcePointsInPatientCoords(),
			matrixParameters.getSourcePointsInPatientCoordsDims()[0],
			matrixParameters.getSourcePointsInPatientCoordsDims()[1]);

	generator.setKernel(matrixParameters.getKernel(), matrixParameters.getKernelDimensions()[0], matrixParameters.getKernelDimensions()[1]);

	generator.setLabelmap(matrixParameters.getLabelmapOffset(), matrixParameters.getLabelmapPixelDimensions(), matrixParameters.getLabelmapDimensions(), matrixParameters.getLabelmap());

	generator.setThreads(numCPU);

	dosemapHostFinal = generator.generateDosemap(xsolHostFinal, xsolHostFinalSize);

	printf("\n\n\n\n--------------------------------------------------------\n");
	printf("Finished generating the dosemap\n");
	printf("Elapsed time : %lf ms\n", toc() / 1000.0f);
	printf("--------------------------------------------------------\n\n\n\n");
}

/*
 * Generate the A matrix from parameters given by Slicer, should happen before optimize()
 */
int ADMMGscon::generateA()
{

	printf("\n\n\n\n--------------------------------------------------------\n");
	printf("Generating the matrix A\n");
	printf("--------------------------------------------------------\n\n\n\n");
	tic();

	/*
	 * Version that actually constructs A, not read from file
	 */
	MatrixGenerator generator;
	generator.setBeamletPointsInPatientCoords(matrixParameters.getBeamletPointsInPatientCoords(),
			matrixParameters.getBeamletPointsInPatientCoordsDims()[0],
			matrixParameters.getBeamletPointsInPatientCoordsDims()[1]);

	generator.setPerpendicularPlaneVectors(matrixParameters.getPerpendicularPlaneVectors(),
			matrixParameters.getPerpendicularPlaneVectorsDims()[0],
			matrixParameters.getPerpendicularPlaneVectorsDims()[1]);

	generator.setSourcePointsInPatientCoords(matrixParameters.getSourcePointsInPatientCoords(),
			matrixParameters.getSourcePointsInPatientCoordsDims()[0],
			matrixParameters.getSourcePointsInPatientCoordsDims()[1]);

	generator.setKernel(matrixParameters.getKernel(), matrixParameters.getKernelDimensions()[0], matrixParameters.getKernelDimensions()[1]);

	generator.setLabelmap(matrixParameters.getLabelmapOffset(), matrixParameters.getLabelmapPixelDimensions(),
			matrixParameters.getLabelmapDimensions(), matrixParameters.getLabelmap());

	generator.setThreads(numCPU);

	generator.setSortAMatrix(true);

	std::vector<result> *result = generator.generateMatrix();

	//std::cout << "Found " << result->size() << " nnz" << std::endl;

	//int matlab = 33060258;
	nnz = result->size();
	Ms = generator.getRows();
	Ns = generator.getCols();

	printf("Matrix A is [%dx%d] with %d nnz\n",Ms,Ns,nnz);

	//if(result->size() != matlab){
		//std::cout << "NNZ was wrong, should have been " << matlab << std::endl;
	//}

	//printf("Converting COO 2 CSR\n");
	//subtic();
	int* cooRowHostPtrA;
	cudaHostAlloc((void**) &cooRowHostPtrA, nnz * sizeof(cooRowHostPtrA[0]),
			cudaHostAllocDefault);

	cudaHostAlloc((void**) &csrColHostPtrA, nnz * sizeof(csrColHostPtrA[0]),
			cudaHostAllocDefault);
	cudaHostAlloc((void**) &csrRowHostPtrA,
			(Ms + 1) * sizeof(csrRowHostPtrA[0]), cudaHostAllocDefault);
	cudaHostAlloc((void**) &csrValHostPtrA, nnz * sizeof(csrValHostPtrA[0]),
			cudaHostAllocDefault);

	for(int i = 0;i<result->size();i++)
	{
		csrValHostPtrA[i] = (*result)[i].value;
		csrColHostPtrA[i] = (*result)[i].x;
		cooRowHostPtrA[i] = (*result)[i].y;
	}

	int* cooRowDevicePtrA;
	cudaMalloc((void**) &cooRowDevicePtrA,nnz * sizeof(cooRowDevicePtrA[0]));
	cudaMemcpy(cooRowDevicePtrA,cooRowHostPtrA,nnz * sizeof(cooRowDevicePtrA[0]),cudaMemcpyHostToDevice);

	int* csrRowDevicePtrA;
	cudaMalloc((void **) &csrRowDevicePtrA,(Ms+1) * sizeof(csrRowDevicePtrA[0]));

	status = cusparseXcoo2csr(handle,cooRowDevicePtrA,nnz,Ms,csrRowDevicePtrA,CUSPARSE_INDEX_BASE_ZERO);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		printf("Matrix COO 2 CSR failed\n");
		return 1;
	}

	cudaMemcpy(csrRowHostPtrA,csrRowDevicePtrA,(Ms+1)*sizeof(csrRowDevicePtrA[0]),cudaMemcpyDeviceToHost);

	if ((!csrRowHostPtrA) || (!csrColHostPtrA) || (!csrValHostPtrA)) {
		printf("Host malloc A failed (matrix)\n");
		return 1;
	}
	//printf("Elapsed time : %lf ms\n", subtoc() / 1000.0f);

	printf("\n\n\n\n--------------------------------------------------------\n");
	printf("Finished generating the matrix A\n");
	printf("Elapsed time : %lf ms\n", toc() / 1000.0f);
	printf("--------------------------------------------------------\n\n\n\n");

	cudaFreeHost(cooRowHostPtrA);
	cudaFree(cooRowDevicePtrA);
	cudaFree(csrRowDevicePtrA);
	delete result;
	return 0;
}

void ADMMGscon::setOptimizationParameters(OptimizationParameters &parameters)
{
	optiParameters = parameters;
}
int ADMMGscon::getXSolSize()
{
	return xsolHostFinalSize;
}
float* ADMMGscon::getXSol()
{
	return xsolHostFinal;
}
int ADMMGscon::getDosemapSize()
{
	return matrixParameters.getLabelmapDimensions()[0]*matrixParameters.getLabelmapDimensions()[1]*
			matrixParameters.getLabelmapDimensions()[2];
}
SOLUTION_PRECISION* ADMMGscon::getDosemap()
{
	return dosemapHostFinal;
}

/*
 * Init various cuda libraries and multi-gpu features, is called by constructor
 * Should ALWAYS be called before doing anything else with cusparse and cublas
 */
int ADMMGscon::init()
{
	/*
	 * find number of cores available on machine, needs OMP enabled
	 */
	numCPU = omp_get_num_procs();
	printf("Found %ld CPU cores available\n",numCPU);

	/*
	 * Init cusparse, cublas libraries
	 */
	status= cusparseCreate(&handle);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		printf("CUSPARSE Library initialization failed\n");
		return 1;
	}
	blasStatus = cublasCreate_v2(&blasHandle);
	if (blasStatus != CUBLAS_STATUS_SUCCESS) {
		printf("CUBLAS Library initialization failed\n");
		return 1;
	}
	/* create and setup matrix descriptor */
	status= cusparseCreateMatDescr(&descr);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		printf("Matrix descriptor initialization failed\n");
		return 1;
	}
	cusparseSetMatType(descr,CUSPARSE_MATRIX_TYPE_GENERAL);
	cusparseSetMatIndexBase(descr,CUSPARSE_INDEX_BASE_ZERO);

	/*
	 * Check if requested # of GPUs is OK and list them inside array
	 */
	printf("Checking for multiple GPUs...\n");
	int gpu_n;
	cudaGetDeviceCount(&gpu_n);
	printf("CUDA-capable device count: %i\n", gpu_n);

	// Query device properties
	cudaDeviceProp prop[64];
	int gpu_count = 0;   // GPUs that meet the criteria

	for (int i=0; i < gpu_n; i++)
	{
		cudaGetDeviceProperties(&prop[i], i);

		// Only boards based on Fermi can support P2P
		if (prop[i].major >= 2)
		{
			// This is an array of P2P capable GPUs
			gpuid[gpu_count++] = i;
		}

		printf("> GPU%d = \"%15s\" %s capable of Peer-to-Peer (P2P)\n", i, prop[i].name, (IsGPUCapableP2P(&prop[i]) ? "IS " : "IS NOT"));
	}

	if(gpu_count < NGPUS)
	{
		printf("Warning, less than %d GPUs (%d) support P2P communication !!!",NGPUS,gpu_count);
		return 1;
	}

	// check for multi-GPU peer access
	int canP2P = 0;
	for(int i = 1;i<NGPUS;i++)
	{
		cudaDeviceCanAccessPeer(&canP2P,gpuid[0],gpuid[i]);
		if(canP2P == 0)
		{
			printf("Error, GPU %d can't P2P access GPU %d\n",gpuid[0],gpuid[i]);
			return 1;
		}
	}
	for(int i = 1;i<NGPUS;i++)
	{
		cudaDeviceCanAccessPeer(&canP2P,gpuid[i],gpuid[0]);
		if(canP2P == 0)
		{
			printf("Error, GPU %d can't P2P access GPU %d\n",gpuid[i],gpuid[0]);
			return 1;
		}
	}

	// enable peer access and create cuda streams
	for(int i = 0;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		for(int j = 0;j<NGPUS-1;j++)
		{
			const int GPUID = (i+j+1)%NGPUS;
			cudaDeviceEnablePeerAccess(gpuid[GPUID],0);
			printf("GPU %d can now P2P access GPU %d\n",gpuid[i],gpuid[GPUID]);
		}
		cudaStreamCreate(&stream[i]);
	}

	cudaSetDevice(gpuid[0]);
	return 0;
}

/*
 * Compute the optimized output vector that represents the dose the patient will get
 */
int ADMMGscon::optimize()
{
	HANDLE_ERROR(cudaPeekAtLastError());
	printf("\n\n\n\n--------------------------------------------------------\n");
	printf("Starting GPU optimization\n");
	printf("--------------------------------------------------------\n\n\n\n");
	globaltic();

	/*
	 * Async copy of A to GPU
	 */

	HANDLE_ERROR(cudaMalloc((void**) &csrColDevicePtrA, nnz * sizeof(csrColDevicePtrA[0])));
	HANDLE_ERROR(cudaMalloc((void**) &csrRowDevicePtrA,
			(Ms + 1) * sizeof(csrRowDevicePtrA[0])));
	HANDLE_ERROR(cudaMalloc((void**) &csrValDevicePtrA, nnz * sizeof(csrValDevicePtrA[0])));

	cudaStreamCreate(&copyAStream);

	HANDLE_ERROR(cudaMemcpyAsync(csrRowDevicePtrA, csrRowHostPtrA,
			(size_t) ((Ms + 1) * sizeof(csrRowDevicePtrA[0])),
			cudaMemcpyHostToDevice, copyAStream));
	HANDLE_ERROR(cudaMemcpyAsync(csrColDevicePtrA, csrColHostPtrA,
			(size_t) (nnz * sizeof(csrColDevicePtrA[0])),
			cudaMemcpyHostToDevice, copyAStream));
	HANDLE_ERROR(cudaMemcpyAsync(csrValDevicePtrA, csrValHostPtrA,
			(size_t) (nnz * sizeof(csrValDevicePtrA[0])),
			cudaMemcpyHostToDevice, copyAStream));

	//printf("Elapsed time : %lf ms\n", toc() / 1000.0f);

	/*
	 * load Y and create Y
	 */

	createYFromLabelmap();

	/*
	 * Create B and augment y
	 */
	createB();

	// y = [y; d8*ones(length(as4),1); d9*ones(length(as5),1); d10*ones(length(as6),1)];
	augmentY();

	printf("Matrix B=[%dx%d] with %d elts\n", Ms, Ns, nnz);

	/*
	 * Split the matrix B
	 */

	int* subNNZB = 0;
	int* subRowNbB = 0;

	int** csrRowDevicePtrSplittedB = 0;
	int** csrColDevicePtrSplittedB = 0;
	float** csrValDevicePtrSplittedB = 0;

	//printf("Splitting the B matrix for %d GPUs\n",NGPUS);
	splitCSRMatrix(Ms,nnz,csrRowDevicePtrB,csrColDevicePtrB,csrValDevicePtrB,
			csrRowDevicePtrSplittedB,csrColDevicePtrSplittedB,csrValDevicePtrSplittedB,
			subNNZB,subRowNbB);

	HANDLE_ERROR(cudaPeekAtLastError());

	/*
	 * Create BT from B
	 */
	//printf("Creating BT from B\n");
	int * cscRowDevicePtrB = 0;
	int * cscColDevicePtrB = 0;
	float * cscValDevicePtrB = 0;

	cudaMalloc((void**) &cscColDevicePtrB,
			(Ns+1) * sizeof(cscColDevicePtrB[0]));
	cudaMalloc((void**) &cscRowDevicePtrB,
			nnz * sizeof(cscRowDevicePtrB[0]));
	cudaMalloc((void**) &cscValDevicePtrB,
			nnz * sizeof(cscValDevicePtrB[0]));
	tic();
	status = cusparseScsr2csc(handle,Ms,Ns,nnz,csrValDevicePtrB,csrRowDevicePtrB,csrColDevicePtrB,
			cscValDevicePtrB,cscRowDevicePtrB,cscColDevicePtrB,CUSPARSE_ACTION_NUMERIC,CUSPARSE_INDEX_BASE_ZERO);
	cudaDeviceSynchronize();
	HANDLE_ERROR(cudaPeekAtLastError());
	//printf("Elapsed time : %lf ms\n", toc() / 1000.0f);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		printf("CSR 2 CSC failed\n");
		return 1;
	}

	/*
	 * Remember that we use CSC A matrix to represent CSR AT matrix.
	 *
	 * This is possible by simply using cscCol as csrRow and cscRow as csrCol
	 */

	/*
	 * Split the matrix BT
	 */

	int* subNNZBT = 0;
	int* subRowNbBT = 0;

	int** cscRowDevicePtrSplittedB = 0;
	int** cscColDevicePtrSplittedB = 0;
	float** cscValDevicePtrSplittedB = 0;

	//printf("Splitting the BT matrix for %d GPUs\n",NGPUS);
	splitCSRMatrix(Ns,nnz,cscColDevicePtrB,cscRowDevicePtrB,cscValDevicePtrB,
			cscColDevicePtrSplittedB,cscRowDevicePtrSplittedB,cscValDevicePtrSplittedB,
			subNNZBT,subRowNbBT);

	/*
	 * create c, size = NG
	 */
	cDevicePtr = 0;
	printf("Creating vector c\n");
	tic();
	cudaMalloc((void**) &cDevicePtr, NG * sizeof(cDevicePtr[0]));
	initArrayKernel<<<(NG+255)/256,256>>>(cDevicePtr,NG,1.0);
	//printf("Elapsed time : %lf ms\n", toc() / 1000.0f);

	float sval = 0;

	pow_method(&sval, Ms, Ns, nnz, cscColDevicePtrB, cscRowDevicePtrB, cscValDevicePtrB,
			csrColDevicePtrB, csrRowDevicePtrB, csrValDevicePtrB, descr, handle, blasHandle);

	/*
	 * init z, size = M
	 */

	zDevicePtr = 0;
	cudaMalloc((void**) &zDevicePtr, Ms * sizeof(zDevicePtr[0]));
	initArrayKernel<<<(Ms+255)/256,256>>>(zDevicePtr,Ms,0.0);

	/*
	 * init xsol, size = N
	 */
	xsolDevicePtr = (float**) malloc(NGPUS*sizeof(*xsolDevicePtr));
	for(int i = 0;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		cudaMalloc((void**) &xsolDevicePtr[i], Ns * sizeof(xsolDevicePtr[i][0]));
	}
	cudaSetDevice(gpuid[0]);
	initArrayKernel<<<(Ns+255)/256,256>>>(xsolDevicePtr[0],Ns,0.0);

	float target_rel_error = optiParameters.getRelativeErrorStage1();
	int max_iter = optiParameters.getMaxIterationsStage1();
	mu = 1.0f/sval;
	float gamma = optiParameters.getGammaStage1()/mu;

	int error = admm(Ms, Ns, NG, NGPUS, sval, subRowNbB, nnz, descr, handle, blasHandle, yDevicePtr,
			stream, cscColDevicePtrSplittedB, cscRowDevicePtrSplittedB, cscValDevicePtrSplittedB,
			csrColDevicePtrSplittedB, csrRowDevicePtrSplittedB, csrValDevicePtrSplittedB,
			subNNZB, subNNZBT, cDevicePtr, xsolDevicePtr, zDevicePtr,
			subRowNbBT, csrRowDevicePtrB, csrColDevicePtrB, csrValDevicePtrB,
			target_rel_error,max_iter,gamma,0,"output_xsol.txt");

	if(error)
	{
		printf("Something wrong with ADMM_gscon, aborting...");
		return error;
	}

	HANDLE_ERROR(cudaPeekAtLastError());

	HANDLE_ERROR(cudaFree(cscColDevicePtrB));
	HANDLE_ERROR(cudaFree(cscRowDevicePtrB));
	HANDLE_ERROR(cudaFree(cscValDevicePtrB));
	for(int i = 0;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		HANDLE_ERROR(cudaFree(csrColDevicePtrSplittedB[i]));
		HANDLE_ERROR(cudaFree(csrRowDevicePtrSplittedB[i]));
		HANDLE_ERROR(cudaFree(csrValDevicePtrSplittedB[i]));
		HANDLE_ERROR(cudaFree(cscColDevicePtrSplittedB[i]));
		HANDLE_ERROR(cudaFree(cscRowDevicePtrSplittedB[i]));
		HANDLE_ERROR(cudaFree(cscValDevicePtrSplittedB[i]));
	}
	cudaSetDevice(gpuid[0]);

	// update host data to use for creating small B matrix
	cudaStreamCreate(&copyAStream);

	HANDLE_ERROR(cudaFreeHost(csrRowHostPtrA));
	HANDLE_ERROR(cudaFreeHost(csrColHostPtrA));
	HANDLE_ERROR(cudaFreeHost(csrValHostPtrA));

	HANDLE_ERROR(cudaHostAlloc((void**) &csrColHostPtrB, nnz * sizeof(csrColHostPtrB[0]),
			cudaHostAllocDefault));
	HANDLE_ERROR(cudaHostAlloc((void**) &csrRowHostPtrB,
			(Ms + 1) * sizeof(csrRowHostPtrB[0]), cudaHostAllocDefault));
	HANDLE_ERROR(cudaHostAlloc((void**) &csrValHostPtrB, nnz * sizeof(csrValHostPtrB[0]),
			cudaHostAllocDefault));

	HANDLE_ERROR(cudaMemcpyAsync(csrRowHostPtrB, csrRowDevicePtrB,
			((Ms + 1) * sizeof(csrRowDevicePtrB[0])),
			cudaMemcpyDeviceToHost, copyAStream));
	HANDLE_ERROR(cudaMemcpyAsync(csrColHostPtrB, csrColDevicePtrB,
			(nnz * sizeof(csrColDevicePtrB[0])),
			cudaMemcpyDeviceToHost, copyAStream));
	HANDLE_ERROR(cudaMemcpyAsync(csrValHostPtrB, csrValDevicePtrB,
			(nnz * sizeof(csrValDevicePtrB[0])),
			cudaMemcpyDeviceToHost, copyAStream));

	resizeMatrix();

	/*
	 * create smallXsol, size = Ns1
	 */
	smallXsolDevicePtr = (float**)malloc(NGPUS*sizeof(*smallXsolDevicePtr));
	for(int i = 0;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		cudaMalloc((void**) &smallXsolDevicePtr[i], Ns1 * sizeof(smallXsolDevicePtr[i][0]));
	}
	cudaSetDevice(gpuid[0]);
	// xsol1 = Xsol(x1>0)
	for(int i = 0;i<NG1;i++)
	{
		int startingColumn = normvecIndexHostPtr[i]*NB;
		cudaMemcpyAsync(smallXsolDevicePtr[0]+(i*NB),xsolDevicePtr[0]+startingColumn,
				NB*sizeof(smallXsolDevicePtr[0][0]),cudaMemcpyDeviceToDevice,stream[0]);
	}

	/*
	 * create c1, size = NG1
	 */
	printf("Creating vector c1\n");
	float * c1DevicePtr = 0;
	cudaMalloc((void**) &c1DevicePtr, NG1 * sizeof(c1DevicePtr[0]));
	initArrayKernel<<<(NG1+255)/256,256>>>(c1DevicePtr,NG1,1.0);

	/*
	 * Create B1T from B1
	 */
	printf("Creating B1T from B1\n");
	tic();

	cudaMalloc((void**) &cscColDevicePtrSmallB,
			(Ns1+1) * sizeof(cscColDevicePtrSmallB[0]));
	cudaMalloc((void**) &cscRowDevicePtrSmallB,
			newNNZ * sizeof(cscRowDevicePtrSmallB[0]));
	cudaMalloc((void**) &cscValDevicePtrSmallB,
			newNNZ * sizeof(cscValDevicePtrSmallB[0]));
	cudaPeekAtLastError();
	cudaDeviceSynchronize();
	status = cusparseScsr2csc(handle,Ms,Ns1,newNNZ,csrValDevicePtrSmallB,csrRowDevicePtrSmallB,csrColDevicePtrSmallB,
			cscValDevicePtrSmallB,cscRowDevicePtrSmallB,cscColDevicePtrSmallB,CUSPARSE_ACTION_NUMERIC,CUSPARSE_INDEX_BASE_ZERO);
	cudaDeviceSynchronize();
	printf("Elapsed time : %lf ms\n", toc() / 1000.0f);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		printf("CSR 2 CSC failed\n");
		return 1;
	}
	cudaPeekAtLastError();
	cudaDeviceSynchronize();

	// sval1 = pow_method(B1...)
	pow_method(&sval, Ms, Ns1, newNNZ, cscColDevicePtrSmallB, cscRowDevicePtrSmallB, cscValDevicePtrSmallB,
			csrColDevicePtrSmallB, csrRowDevicePtrSmallB, csrValDevicePtrSmallB, descr, handle, blasHandle);

	printf("sval1: %f\n",sval);

	cudaPeekAtLastError();
	cudaDeviceSynchronize();

	cudaStreamDestroy(copyAStream);
	cudaStreamSynchronize(stream[0]);
	cudaFree(csrRowDevicePtrB);
	cudaFree(csrColDevicePtrB);
	cudaFree(csrValDevicePtrB);
	printf("Elapsed time : %lf ms\n", toc() / 1000.0f);

	/*
	 * Split the matrix B1
	 */

	int* subNNZSmallB = 0;
	int* subRowNbSmallB = 0;

	printf("Splitting the B1 matrix for %d GPUs\n",NGPUS);
	splitCSRMatrix(Ms,newNNZ,csrRowDevicePtrSmallB,csrColDevicePtrSmallB,csrValDevicePtrSmallB,
			csrRowDevicePtrSplittedSmallB,csrColDevicePtrSplittedSmallB,csrValDevicePtrSplittedSmallB,
			subNNZSmallB,subRowNbSmallB);

	/*
	 * Remember that we use CSC A matrix to represent CSR AT matrix.
	 *
	 * This is possible by simply using cscCol as csrRow and cscRow as csrCol
	 */

	/*
	 * Split the matrix B1T
	 */

	int* subNNZSmallBT = 0;
	int* subRowNbSmallBT = 0;

	printf("Splitting the B1T matrix for %d GPUs\n",NGPUS);
	splitCSRMatrix(Ns1,newNNZ,cscColDevicePtrSmallB,cscRowDevicePtrSmallB,cscValDevicePtrSmallB,
			cscColDevicePtrSplitSB,cscRowDevicePtrSplitSB,cscValDevicePtrSplitSB,
			subNNZSmallBT,subRowNbSmallBT);

	target_rel_error = optiParameters.getRelativeErrorStage2();
	max_iter = optiParameters.getMaxIterationsStage2();
	mu = 1.0f/sval;
	gamma = optiParameters.getGammaStage2()/mu;
	initZ = false;

	error = admm(Ms, Ns1, NG1, NGPUS, sval, subRowNbSmallB, newNNZ, descr, handle, blasHandle, yDevicePtr,
			stream, cscColDevicePtrSplitSB, cscRowDevicePtrSplitSB, cscValDevicePtrSplitSB,
			csrColDevicePtrSplittedSmallB, csrRowDevicePtrSplittedSmallB, csrValDevicePtrSplittedSmallB,
			subNNZSmallB, subNNZSmallBT, c1DevicePtr, smallXsolDevicePtr, zDevicePtr,
			subRowNbSmallBT, csrRowDevicePtrSmallB, csrColDevicePtrSmallB, csrValDevicePtrSmallB,
			target_rel_error,max_iter,gamma,0,"output_xsol2.txt");

	if(error)
	{
		printf("Something wrong with ADMM_gscon, aborting...");
		return error;
	}
	HANDLE_ERROR(cudaPeekAtLastError());
	cudaDeviceSynchronize();
	cudaSetDevice(gpuid[0]);

	/*
	 * save XSOL
	 */

	// Xsol2 = Xsol2(1:Ns2)
	float* tempXsolHostFinal = (float*)malloc(Ns1*sizeof(*tempXsolHostFinal));
	cudaMemcpy(tempXsolHostFinal,smallXsolDevicePtr[0],Ns1*sizeof(*tempXsolHostFinal),cudaMemcpyDeviceToHost);

	// Xsol22 = zeros(size(Xsol))
	xsolHostFinal = (float*)malloc(Ns*sizeof(*xsolHostFinal));
	xsolHostFinalSize = Ns;
	memset(xsolHostFinal,0,Ns*sizeof(*xsolHostFinal));

	int sourceIndex = 0;
	for(int i = 0;i<NG1;i++)
	{
		int targetCol = normvecIndexHostPtr[i]*NB;
		memcpy(xsolHostFinal+targetCol,tempXsolHostFinal+sourceIndex,NB*sizeof(*xsolHostFinal));
		sourceIndex+=NB;
	}

	free(tempXsolHostFinal);
	HANDLE_ERROR(cudaPeekAtLastError());
	/*
	 * END
	 */

	printf("End of the program, cleaning up memory\n");
	clean();

	printf("Memory usage\n");
	for(int i = 0;i<NGPUS;i++)
	{
		showGPUMemory(i);
	}

	printf("\n\n\n\n--------------------------------------------------------\n");
	printf("Finished optimization\n");

	for(int i = 0;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		cudaDeviceSynchronize();
		cudaDeviceReset();
	}
	cudaSetDevice(gpuid[0]);
	HANDLE_ERROR(cudaPeekAtLastError());

	printf("Elapsed time : %lf ms\n", globaltoc() / 1000.0f);
	printf("--------------------------------------------------------\n\n\n\n");

	return 0;
}

/*
 * Before doing stage 2 of ADMM algorithm, resize matrix keeping most important columns
 */
void ADMMGscon::resizeMatrix()
{
	printf("Computing number of shots\n");
	tic();
	// Compute the L2 norm per col, size NG
	float* normvecDevicePtr = 0;
	HANDLE_ERROR(cudaMalloc((void**) &normvecDevicePtr, NG * sizeof(normvecDevicePtr[0])));
	HANDLE_ERROR(cudaPeekAtLastError());

	const int MAX_BLOCKS = 65536;

	if(NG > MAX_BLOCKS)
		printf("Warning with number of blocks launched for l2NormVector in l2l1_prox\n");
	l2NormVector<<<NG,256>>>(normvecDevicePtr,xsolDevicePtr[0],NB,NG);
	HANDLE_ERROR(cudaPeekAtLastError());
	cudaDeviceSynchronize();
	float* normvecHostPtr = 0;
	normvecHostPtr = (float*) malloc(NG * sizeof(normvecHostPtr[0]));
	cudaMemcpy(normvecHostPtr,normvecDevicePtr,NG * sizeof(normvecDevicePtr[0]),cudaMemcpyDeviceToHost);
	HANDLE_ERROR(cudaPeekAtLastError());
	cudaDeviceSynchronize();

	int d = 0;
	for(int i = 0;i<NG;i++)
	{
		if(normvecHostPtr[i] > 0)
			d+=1;
	}
	printf("Elapsed time : %lf ms\n", toc() / 1000.0f);
	printf("Sparse coefficients. Numb. of shots=%d\n",d);



	printf("Creating new smaller B matrix\n");
	tic();

	subtic();
	normvecIndexHostPtr = 0;

	normvecIndexHostPtr = (int*) malloc(NG * sizeof(normvecIndexHostPtr[0]));
	for(int i = 0;i<NG;i++)
	{
		normvecIndexHostPtr[i]= i;
	}
	globalNormvecHostPtr = normvecHostPtr;
	// [normsoted, ind1] = sort(normvec,'descend');
	std::sort(normvecIndexHostPtr, normvecIndexHostPtr+NG, sortDescendComparator);
	NG1 = 0;
	if(d > 10)
		NG1 = 10;
	else
		NG1 = d;
	// normvecIndex should be cut to NG1 length, but can avoid it with pointers
	// ind2 = ind1(1:NG1), ind2 = sort(ind2)
	std::sort(normvecIndexHostPtr, normvecIndexHostPtr+NG1,sortAscendComparator);

	int* normvecIndexDevicePtr = 0;
	cudaMalloc((void **)&normvecIndexDevicePtr,NG1*sizeof(normvecIndexDevicePtr[0]));
	cudaPeekAtLastError();
	cudaDeviceSynchronize();
	cudaMemcpy(normvecIndexDevicePtr,normvecIndexHostPtr,NG1*sizeof(normvecIndexDevicePtr[0]),cudaMemcpyHostToDevice);
	cudaPeekAtLastError();
	cudaDeviceSynchronize();

	Ns1 = NG1*NB;
	printf("Elapsed time : %lf ms\n", subtoc() / 1000.0f);
	printf("New matrix B will have %d columns\n",Ns1);

	subtic();
	int* intermediateNNZDevicePtr = 0;
	cudaMalloc((void **)&intermediateNNZDevicePtr,NG1*sizeof(intermediateNNZDevicePtr[0]));
	// count new number of non zeros
	resizeBCountNNZ<<<NG1,256>>>(csrColDevicePtrB, normvecIndexDevicePtr, intermediateNNZDevicePtr, NB, nnz);
	cudaPeekAtLastError();
	cudaDeviceSynchronize();

	int* intermediateNNZHostPtr = 0;
	intermediateNNZHostPtr = (int*) malloc(NG1 * sizeof(intermediateNNZHostPtr[0]));
	cudaMemcpy(intermediateNNZHostPtr,intermediateNNZDevicePtr,NG1*sizeof(intermediateNNZHostPtr[0]),cudaMemcpyDeviceToHost);
	for(int i = 0;i<NG1;i++)
	{
		newNNZ+=intermediateNNZHostPtr[i];
	}
	//printf("Elapsed time : %lf ms\n", subtoc() / 1000.0f);
	//printf("New matrix B will have %d nnz\n",newNNZ);

	subtic();

	cudaMalloc((void**) &csrColDevicePtrSmallB, newNNZ * sizeof(csrColDevicePtrSmallB[0]));
	cudaMalloc((void**) &csrRowDevicePtrSmallB,
			(Ms + 1) * sizeof(csrRowDevicePtrSmallB[0]));
	cudaMalloc((void**) &csrValDevicePtrSmallB, newNNZ * sizeof(csrValDevicePtrSmallB[0]));

	cudaStreamSynchronize(copyAStream); // if update of host from device not finished, wait

	int srcCopyEltIndex = 0;
	int dstCopyEltIndex = 0;
	int nbOfEltsToCopy = 0;
	int nbOfEltsFound = 0;
	int crtCsrRowIndex = 0;
	int crtGroupBigMatrix = 0;
	cudaMemcpyAsync(csrRowDevicePtrSmallB+crtCsrRowIndex,&nbOfEltsFound,
			sizeof(csrRowDevicePtrSmallB[0]),cudaMemcpyHostToDevice);
	// check if the first lines are empty
	while(csrRowHostPtrB[crtCsrRowIndex] == csrRowHostPtrB[crtCsrRowIndex+1])
	{
		crtCsrRowIndex++;
		cudaMemcpyAsync(csrRowDevicePtrSmallB+crtCsrRowIndex,&nbOfEltsFound,
				sizeof(csrRowDevicePtrSmallB[0]),cudaMemcpyHostToDevice);
	}
	crtCsrRowIndex++;
	// B1 = B(:,x1>0)
	for(int i = 0;i<nnz;i++)
	{
		// new line(s)
		while(i == csrRowHostPtrB[crtCsrRowIndex])
		{
			cudaMemcpyAsync(csrRowDevicePtrSmallB+crtCsrRowIndex,&nbOfEltsFound,
					sizeof(csrRowDevicePtrSmallB[0]),cudaMemcpyHostToDevice);
			crtCsrRowIndex++;
		}
		crtGroupBigMatrix = (csrColHostPtrB[i]/NB)%NG;
		for(int normvecIndex = 0; normvecIndex < NG1; normvecIndex++)
		{
			if(crtGroupBigMatrix == normvecIndexHostPtr[normvecIndex])
			{
				srcCopyEltIndex = i;
				nbOfEltsToCopy = 1;
				nbOfEltsFound++;

				int nextEltsIndex = i+1;
				int nextGroupBigMatrix = (csrColHostPtrB[nextEltsIndex]/NB)%NG;

				while(nextGroupBigMatrix == crtGroupBigMatrix && nextEltsIndex < csrRowHostPtrB[crtCsrRowIndex])
				{
					nbOfEltsToCopy++;
					nbOfEltsFound++;
					nextEltsIndex++;
					nextGroupBigMatrix = (csrColHostPtrB[nextEltsIndex]/NB)%NG;
				}

				int groupIndexInBigMatrix = normvecIndexHostPtr[normvecIndex];
				int groupIndexInNewMatrix = normvecIndex;
				int offset = groupIndexInBigMatrix-groupIndexInNewMatrix;
				if(offset > 0){
					offset *= NB;
				}else{ //Find out if offset can actually be smaller than 0
					offset = 0;
				}

				//Copy csrCol and csrVal from old matrix to new one. csrCol will have offset substracted for every value
				asyncSplitKernel<<<(nbOfEltsToCopy+127)/128, 128, 0>>>(csrColDevicePtrSmallB+dstCopyEltIndex,csrColDevicePtrB+srcCopyEltIndex,
						csrValDevicePtrSmallB+dstCopyEltIndex,csrValDevicePtrB+srcCopyEltIndex,
						nbOfEltsToCopy, offset);

				dstCopyEltIndex+=nbOfEltsToCopy;

				//printf("Copy %d\n", nbOfEltsToCopy);
				i = nextEltsIndex-1;
				break;
			}
		}
	}
	while(crtCsrRowIndex < Ms)
	{
		cudaMemcpyAsync(csrRowDevicePtrSmallB+crtCsrRowIndex,&nbOfEltsFound,
				sizeof(csrRowDevicePtrSmallB[0]),cudaMemcpyHostToDevice);
		crtCsrRowIndex++;
	}
	cudaMemcpyAsync(csrRowDevicePtrSmallB+crtCsrRowIndex,&nbOfEltsFound,
			sizeof(csrRowDevicePtrSmallB[0]),cudaMemcpyHostToDevice);

	//cudaStreamSynchronize(stream[0]); // we need CSR device ptrs to be ready
	cudaDeviceSynchronize();
	printf("Elapsed time : %lf ms\n", subtoc() / 1000.0f);
	printf("Elapsed time : %lf ms\n", toc() / 1000.0f);
	printf("%d elements found to copy, should equal %d\n",nbOfEltsFound,newNNZ);
}

/*
 * Create Y vector by keeping values from labelmap bigger than threshold
 */
void ADMMGscon::createYFromLabelmap()
{
	printf("Creating Y\n");
	tic();
	yLabelMapDevicePtr = 0;
	// y host and device, size = M (sizeOfY)
	yDevicePtr = 0;
	float * yHostPtr = 0;

	if(matrixParameters.getLabelmap() != 0 && matrixParameters.getLabelmapDimensions() != 0)
	{
		labelMapHostPtr = matrixParameters.getLabelmap();
		labelMapSize = matrixParameters.getLabelmapDimensions()[0]*matrixParameters.getLabelmapDimensions()[1]*
				matrixParameters.getLabelmapDimensions()[2];
		printf("Input labelmap has size [%dx%dx%d]=%d\n",matrixParameters.getLabelmapDimensions()[0],
				matrixParameters.getLabelmapDimensions()[1],matrixParameters.getLabelmapDimensions()[2],labelMapSize);
	}
	else
	{
		printf("Error, labelMap not set, aborting...");
		exit(1);
	}

	int newLabelMapSize = 0;
	// threshold set by optimization parameters
	int cutYThreshold = optiParameters.getRegionIDs()[0];
	// Y = labelmap(labelmap>=2)
	for(int i = 0;i<labelMapSize;i++)
	{
		if(labelMapHostPtr[i]>=cutYThreshold)
			newLabelMapSize++;
	}
	printf("Y has size %d with threshold %d\n",newLabelMapSize,cutYThreshold);
	int* newLabelMapHostPtr = (int*)malloc(newLabelMapSize*sizeof(*newLabelMapHostPtr));
	int newLabelMapID = 0;
	for(int i = 0;i<labelMapSize;i++)
	{
		if(labelMapHostPtr[i]>=cutYThreshold)
			newLabelMapHostPtr[newLabelMapID++] = labelMapHostPtr[i];
	}
	printf("new Y is %d, should be %d\n",newLabelMapID,Ms);
	labelMapSize = newLabelMapSize;

	labelMapHostPtr = newLabelMapHostPtr;

	yHostPtr = (float *) malloc(labelMapSize * sizeof(yHostPtr[0]));
	for (int i = 0; i < labelMapSize; i++) {
		yHostPtr[i] = 1.0;
	}
	HANDLE_ERROR(cudaMalloc((void**) &yDevicePtr,labelMapSize * sizeof(yDevicePtr[0])));
	HANDLE_ERROR(cudaMalloc((void**) &yLabelMapDevicePtr,labelMapSize * sizeof(yLabelMapDevicePtr[0])));

	HANDLE_ERROR(cudaMemcpy(yDevicePtr, yHostPtr,(size_t) (labelMapSize * sizeof(yDevicePtr[0])), cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(yLabelMapDevicePtr, labelMapHostPtr,(size_t) (labelMapSize * sizeof(yLabelMapDevicePtr[0])),
			cudaMemcpyHostToDevice));

	int** linesIDDevicePtr;
	linesIDDevicePtr = (int**)malloc(optiParameters.getRegionTypes()*sizeof(*linesIDDevicePtr));

	size_t linesIDSize = labelMapSize * sizeof(linesIDDevicePtr[0][0]);
	HANDLE_ERROR(cudaPeekAtLastError());

	//Count amount of regions defining a lower bound
	lowerBoundRegions = 0;
	for(int i = 0; i < optiParameters.getRegionTypes(); i++){
		float lowerBound = optiParameters.getRegionLowerBounds()[i];
		if(lowerBound != -1){
			lowerBoundRegions++;
		}
	}
	//Find the ids of the lower bound regions
	lowerBoundRegionIDs = (int*)malloc(lowerBoundRegions*sizeof(*lowerBoundRegionIDs));

	//TODO: this is neither efficient nor pretty (see loop just above), but will do for now
	int lowerBoundIndex = 0;
	for(int i = 0; i < optiParameters.getRegionTypes(); i++){
		float lowerBound = optiParameters.getRegionLowerBounds()[i];
		if(lowerBound != -1){
			lowerBoundRegionIDs[lowerBoundIndex++] = i;
		}
	}

	// getRegionTypes() gives number of dX : d1 -> d7
	for(int i = 0;i<optiParameters.getRegionTypes();i++)
	{
		cudaMalloc((void**) &linesIDDevicePtr[i],linesIDSize);
		cudaMemset(linesIDDevicePtr[i],0,linesIDSize);
		findAndReplaceInVector<<<(labelMapSize + 255) / 256, 256>>>(
				yDevicePtr, yLabelMapDevicePtr, optiParameters.getRegionIDs()[i],
				optiParameters.getRegionUpperBounds()[i], labelMapSize,linesIDDevicePtr[i]);
	}

	cudaDeviceSynchronize();
	HANDLE_ERROR(cudaPeekAtLastError());

	linesIDHostPtr = (int**)malloc(optiParameters.getRegionTypes()*sizeof(*linesIDHostPtr));
	for(int i = 0;i<optiParameters.getRegionTypes();i++)
	{
		linesIDHostPtr[i] = new int[labelMapSize];
		HANDLE_ERROR(cudaMemcpy(linesIDHostPtr[i],linesIDDevicePtr[i],linesIDSize,cudaMemcpyDeviceToHost));
	}

	printf("Elapsed time : %lf ms\n", toc() / 1000.0f);
}

/*
 * Free memory and destroy library related vars
 */
int ADMMGscon::clean()
{
	/* destroy matrix descriptor */
	status = cusparseDestroyMatDescr(descr);
	descr = 0;
	if (status != CUSPARSE_STATUS_SUCCESS) {
		printf("Matrix descriptor destruction failed\n");
		return 1;
	}

	/* destroy cusparse handle */
	status = cusparseDestroy(handle);
	handle = 0;
	if (status != CUSPARSE_STATUS_SUCCESS) {
		printf("CUSPARSE Library release of resources failed\n");
		return 1;
	}

	/* destroy cublas handle */
	blasStatus = cublasDestroy_v2(blasHandle);
	blasHandle = 0;
	if (blasStatus != CUBLAS_STATUS_SUCCESS) {
		printf("CUBLAS Library release of resources failed\n");
		return 1;
	}

	HANDLE_ERROR(cudaPeekAtLastError());

	cudaFreeHost(csrColHostPtrB);
	cudaFreeHost(csrValHostPtrB);
	cudaFreeHost(csrRowHostPtrB);

	HANDLE_ERROR(cudaPeekAtLastError());

	if (xsolDevicePtr)
	{
		for(int i = 0;i<NGPUS;i++)
		{
			cudaSetDevice(gpuid[i]);
			if (xsolDevicePtr[i])
				cudaFree(xsolDevicePtr[i]);
		}
	}

	HANDLE_ERROR(cudaPeekAtLastError());

	if (smallXsolDevicePtr)
	{
		for(int i = 0;i<NGPUS;i++)
		{
			cudaSetDevice(gpuid[i]);
			if (smallXsolDevicePtr[i])
			{
				cudaFree(smallXsolDevicePtr[i]);
				smallXsolDevicePtr[i] = 0;
			}
		}
	}

	HANDLE_ERROR(cudaPeekAtLastError());

	if (csrRowDevicePtrSplittedSmallB && csrColDevicePtrSplittedSmallB
			&& csrValDevicePtrSplittedSmallB)
	{
		for(int i = 0;i<NGPUS;i++)
		{
			cudaSetDevice(gpuid[i]);
			if (csrRowDevicePtrSplittedSmallB[i])
				cudaFree(csrRowDevicePtrSplittedSmallB[i]);
			if (csrColDevicePtrSplittedSmallB[i])
				cudaFree(csrColDevicePtrSplittedSmallB[i]);
			if (csrValDevicePtrSplittedSmallB[i])
				cudaFree(csrValDevicePtrSplittedSmallB[i]);
		}
	}

	HANDLE_ERROR(cudaPeekAtLastError());

	if (cscRowDevicePtrSplitSB && cscColDevicePtrSplitSB
			&& cscValDevicePtrSplitSB)
	{
		for(int i = 0;i<NGPUS;i++)
		{
			cudaSetDevice(gpuid[i]);
			if (cscRowDevicePtrSplitSB[i])
				cudaFree(cscRowDevicePtrSplitSB[i]);
			if (cscColDevicePtrSplitSB[i])
				cudaFree(cscColDevicePtrSplitSB[i]);
			if (cscValDevicePtrSplitSB[i])
				cudaFree(cscValDevicePtrSplitSB[i]);
		}
	}

	cudaSetDevice(gpuid[0]);
	HANDLE_ERROR(cudaPeekAtLastError());

	if (cDevicePtr)
	{
		HANDLE_ERROR(cudaFree(cDevicePtr));
		cDevicePtr = 0;
	}
	if (zDevicePtr)
	{
		HANDLE_ERROR(cudaFree(zDevicePtr));
		zDevicePtr = 0;
	}
	if(csrColDevicePtrSmallB)
	{
		HANDLE_ERROR(cudaFree(csrColDevicePtrSmallB));
		csrColDevicePtrSmallB = 0;
	}
	if(csrRowDevicePtrSmallB)
	{
		HANDLE_ERROR(cudaFree(csrRowDevicePtrSmallB));
		csrRowDevicePtrSmallB = 0;
	}
	if(csrValDevicePtrSmallB)
	{
		HANDLE_ERROR(cudaFree(csrValDevicePtrSmallB));
		csrValDevicePtrSmallB = 0;
	}
	if(cscColDevicePtrSmallB)
	{
		HANDLE_ERROR(cudaFree(cscColDevicePtrSmallB));
		cscColDevicePtrSmallB = 0;
	}
	if(cscRowDevicePtrSmallB)
	{
		HANDLE_ERROR(cudaFree(cscRowDevicePtrSmallB));
		cscRowDevicePtrSmallB = 0;
	}
	if(cscValDevicePtrSmallB)
	{
		HANDLE_ERROR(cudaFree(cscValDevicePtrSmallB));
		cscValDevicePtrSmallB = 0;
	}

	HANDLE_ERROR(cudaPeekAtLastError());
	return 0;
}

/*
 * Augment vector y with X new data blocks, X given by ADMM input parameters
 */
void ADMMGscon::augmentY()
{
	printf("Augmenting y\n");
	tic();
	// creating [d8*ones(length(as4),1); d9*ones(length(as5),1); d10*ones(length(as6),1)]
	float* augmYDevicePtr = 0;

	int totalNbLines = 0;
	for(int i = 0; i < lowerBoundRegions; i++){
		totalNbLines += nbOfNewLines[i];
	}

	cudaMalloc((void**) &augmYDevicePtr,totalNbLines * sizeof(augmYDevicePtr[0]));

	//Go through all lower bound regions, only use those that don't have a -1 value
	int negativeRegionsOffset = 0;
	for(int i = 0; i < lowerBoundRegions; i++){
		float lowerBound = optiParameters.getRegionLowerBounds()[lowerBoundRegionIDs[i]];
		initArrayKernel<<<(nbOfNewLines[i]+255)/256,256>>>(augmYDevicePtr + negativeRegionsOffset,nbOfNewLines[i],lowerBound*-1.0f);
		negativeRegionsOffset += nbOfNewLines[i];
	}

	float* tempYDevicePtr = 0;
	HANDLE_ERROR(cudaMalloc((void**) &tempYDevicePtr,(totalNbLines+Ms1) * sizeof(tempYDevicePtr[0])));
	HANDLE_ERROR(cudaDeviceSynchronize());
	HANDLE_ERROR(cudaPeekAtLastError());
	HANDLE_ERROR(cudaMemcpy(tempYDevicePtr,yDevicePtr,Ms1*sizeof(*tempYDevicePtr),cudaMemcpyDeviceToDevice));
	HANDLE_ERROR(cudaMemcpy(tempYDevicePtr+Ms1,augmYDevicePtr,totalNbLines*sizeof(*tempYDevicePtr),cudaMemcpyDeviceToDevice));

	HANDLE_ERROR(cudaFree(yDevicePtr));
	yDevicePtr = tempYDevicePtr;
	tempYDevicePtr = NULL;

	cudaDeviceSynchronize();

	cudaStreamDestroy(copyAStream);
	cudaFree(yLabelMapDevicePtr);

	printf("Elapsed time : %lf ms\n", toc() / 1000.0f);
}

/*
 * Augment matrix A to create new matrix B
 */
void ADMMGscon::createB()
{
	printf("Creating B\n");
	tic();

	// Number of groups (collimators)
	NG = optiParameters.getBeamCount();
	// Number of beamlets per group
	NB = optiParameters.getBeamWidth()*optiParameters.getBeamHeight();

	Ms1 = Ms;

	HANDLE_ERROR(cudaStreamSynchronize(copyAStream)); // if copy not finished, wait

	float augmentedSign = -1.0f;
	// number of ones we will have to add to vector y

	nbOfNewLines = new int[lowerBoundRegions];

	HANDLE_ERROR(cudaPeekAtLastError());

	for(int i = 0; i < lowerBoundRegions; i++){
		int nbOfNewLinesLocal = 0;

		appendLinesToMatrix(csrRowDevicePtrA,csrColDevicePtrA,csrValDevicePtrA,
				csrRowDevicePtrB,csrColDevicePtrB,csrValDevicePtrB,
				linesIDHostPtr[lowerBoundRegionIDs[i]],augmentedSign,nbOfNewLinesLocal);
		// if new append is required, must be applied on PtrA
		csrRowDevicePtrA = csrRowDevicePtrB;
		csrRowDevicePtrB = 0;
		csrColDevicePtrA = csrColDevicePtrB;
		csrColDevicePtrB = 0;
		csrValDevicePtrA = csrValDevicePtrB;
		csrValDevicePtrB = 0;

		nbOfNewLines[i] = nbOfNewLinesLocal;
	}

	// nomore append required, we validate output ptrB
	csrRowDevicePtrB = csrRowDevicePtrA;
	csrRowDevicePtrA = 0;
	csrColDevicePtrB = csrColDevicePtrA;
	csrColDevicePtrA = 0;
	csrValDevicePtrB = csrValDevicePtrA;
	csrValDevicePtrA = 0;
	printf("Elapsed time : %lf ms\n", toc() / 1000.0f);
}

/*
 * Compute the number of elts on the matrix line, given the CSR vector and lineID, lineID in [0;Ms-1]
 */
int ADMMGscon::nbOfEltOfCsrLine(int* csrRowHostPtr, int lineID)
{
	return csrRowHostPtr[lineID+1]-csrRowHostPtr[lineID];
}

/* 
 * Augment input matrix A with lines of A to create B
 */
void ADMMGscon::appendLinesToMatrix(int* &csrRowDevicePtrA,int* &csrColDevicePtrA,float* &csrValDevicePtrA,
		int* &csrRowDevicePtrB,int* &csrColDevicePtrB,float* &csrValDevicePtrB,
		int* linesIDHostPtr,float augmentedSign, int &nbOfNewLines)
{
	//printf("Appending new lines to matrix A to create B\n");
	cudaStream_t copyStream;
	cudaStreamCreate(&copyStream);
	const int BUFFER_SIZE = 10e6;
	int* colDeviceBuffer;
	cudaMalloc((void**) &colDeviceBuffer,BUFFER_SIZE * sizeof(*colDeviceBuffer));
	float* valDeviceBuffer;
	cudaMalloc((void**) &valDeviceBuffer,BUFFER_SIZE * sizeof(*valDeviceBuffer));
	int bufferIndex = 0;
	int* lineBuffer = new int[Ms1]; // we can't add more than current nb of lines, no need for huge buffer
	int lineBufferIndex = 0;
	int newNNZ = nnz;
	for(int i = 0;i<Ms1;i++)
	{
		int crtLine = 0;
		if(linesIDHostPtr[i] != 0)
		{
			crtLine = i; // we must add crtLine to the end of matrix
			// find crtLine in csrRow and check it's not empty
			int nbElts = 0;
			if((nbElts = nbOfEltOfCsrLine(csrRowHostPtrA,crtLine)) > 0)
			{
				// empty buffer first
				if(bufferIndex+nbElts > BUFFER_SIZE)
				{
					newNNZ += bufferIndex;
					// csrCol new size (old + buffer)
					cudaMalloc((void**) &csrColDevicePtrB,newNNZ * sizeof(*csrColDevicePtrB));
					cudaMemcpy(csrColDevicePtrB,csrColDevicePtrA,nnz*sizeof(*csrColDevicePtrB),cudaMemcpyDeviceToDevice);
					cudaFree(csrColDevicePtrA);
					csrColDevicePtrA = csrColDevicePtrB;
					// csrVal new size (old + buffer)
					cudaMalloc((void**) &csrValDevicePtrB,newNNZ * sizeof(*csrValDevicePtrB));
					cudaMemcpy(csrValDevicePtrB,csrValDevicePtrA,nnz*sizeof(*csrValDevicePtrB),cudaMemcpyDeviceToDevice);
					cudaFree(csrValDevicePtrA);
					csrValDevicePtrA = csrValDevicePtrB;
					cudaStreamSynchronize(copyStream);
					cudaMemcpyAsync(csrColDevicePtrA+nnz,colDeviceBuffer,bufferIndex*sizeof(*csrColDevicePtrA),cudaMemcpyDeviceToDevice,copyStream);
					if(augmentedSign < 0)
						flipSignVector<<<(bufferIndex+255)/256,256>>>(valDeviceBuffer,bufferIndex);
					cudaMemcpyAsync(csrValDevicePtrA+nnz,valDeviceBuffer,bufferIndex*sizeof(*csrValDevicePtrA),cudaMemcpyDeviceToDevice,copyStream);
					bufferIndex = 0;
					nnz = newNNZ;
				}
				int offset = csrRowHostPtrA[crtLine];
				cudaMemcpyAsync(colDeviceBuffer+bufferIndex,csrColDevicePtrA+offset,nbElts*sizeof(*colDeviceBuffer),cudaMemcpyDeviceToDevice,copyStream);
				cudaMemcpyAsync(valDeviceBuffer+bufferIndex,csrValDevicePtrA+offset,nbElts*sizeof(*valDeviceBuffer),cudaMemcpyDeviceToDevice,copyStream);
				bufferIndex += nbElts;
				if(lineBufferIndex == 0)
				{
					lineBuffer[lineBufferIndex] = csrRowHostPtrA[Ms]+nbElts;
				}
				else
				{
					lineBuffer[lineBufferIndex] = lineBuffer[lineBufferIndex-1]+nbElts;
				}
				lineBufferIndex++;
			}
		}
	}
	// empty buffer if not already
	if(bufferIndex > 0)
	{
		newNNZ += bufferIndex;
		cudaMalloc((void**) &csrColDevicePtrB,newNNZ * sizeof(*csrColDevicePtrB));
		cudaMalloc((void**) &csrValDevicePtrB,newNNZ * sizeof(*csrValDevicePtrB));
		cudaMemcpy(csrColDevicePtrB,csrColDevicePtrA,nnz*sizeof(*csrColDevicePtrB),cudaMemcpyDeviceToDevice);
		cudaFree(csrColDevicePtrA);
		csrColDevicePtrA = csrColDevicePtrB;
		cudaMemcpy(csrValDevicePtrB,csrValDevicePtrA,nnz*sizeof(*csrValDevicePtrB),cudaMemcpyDeviceToDevice);
		cudaFree(csrValDevicePtrA);
		csrValDevicePtrA = csrValDevicePtrB;
		cudaMemcpyAsync(csrColDevicePtrA+nnz,colDeviceBuffer,bufferIndex*sizeof(*csrColDevicePtrA),cudaMemcpyDeviceToDevice,copyStream);
		if(augmentedSign < 0)
			flipSignVector<<<(bufferIndex+255)/256,256>>>(valDeviceBuffer,bufferIndex);
		cudaMemcpyAsync(csrValDevicePtrA+nnz,valDeviceBuffer,bufferIndex*sizeof(*csrValDevicePtrA),cudaMemcpyDeviceToDevice,copyStream);
		nnz = newNNZ;
	}
	cudaMalloc((void**) &csrRowDevicePtrB,(Ms+1+lineBufferIndex) * sizeof(*csrRowDevicePtrB));
	cudaMemcpy(csrRowDevicePtrB,csrRowDevicePtrA,(Ms+1)*sizeof(*csrRowDevicePtrB),cudaMemcpyDeviceToDevice);
	cudaMemcpy(csrRowDevicePtrB+Ms+1,lineBuffer,lineBufferIndex*sizeof(*csrRowDevicePtrB),cudaMemcpyHostToDevice);
	nbOfNewLines = lineBufferIndex;
	Ms+=lineBufferIndex;
	cudaStreamSynchronize(copyStream);
	cudaStreamDestroy(copyStream);
	// do not free because space is used by csrDevicePtrB
	csrColDevicePtrA = NULL;
	csrValDevicePtrA = NULL;
	cudaFree(colDeviceBuffer);
	cudaFree(valDeviceBuffer);
	// update host arrays
	cudaFreeHost(csrColHostPtrA);
	cudaFreeHost(csrRowHostPtrA);
	cudaFreeHost(csrValHostPtrA);
	cudaHostAlloc((void**) &csrColHostPtrA, nnz * sizeof(csrColHostPtrA[0]),
			cudaHostAllocDefault);
	cudaHostAlloc((void**) &csrRowHostPtrA,
			(Ms + 1) * sizeof(csrRowHostPtrA[0]), cudaHostAllocDefault);
	cudaHostAlloc((void**) &csrValHostPtrA, nnz * sizeof(csrValHostPtrA[0]),
			cudaHostAllocDefault);
	cudaMemcpy(csrRowHostPtrA,csrRowDevicePtrB,(Ms + 1) * sizeof(csrRowHostPtrA[0]),cudaMemcpyDeviceToHost);
	cudaMemcpy(csrColHostPtrA,csrColDevicePtrB,nnz * sizeof(csrColHostPtrA[0]),cudaMemcpyDeviceToHost);
	cudaMemcpy(csrValHostPtrA,csrValDevicePtrB,nnz * sizeof(csrValHostPtrA[0]),cudaMemcpyDeviceToHost);
}

/*
 * cutAtIndex is the index of the elt that should go in splitted array
 *
 * [1,2;3,4] --> [1,2] [3,4] --> cutAtIndex == 2
 *
 * return negative index if not line start, positive index in csrRow otherwise
 *
 * return M if cutAtIndex too big and nothing was found (returning M == error)
 */
int ADMMGscon::checkIfIndexIsLineStart(int cutAtIndex,int* csrRowHostPtr,int M)
{
	for(int i=0;i<M;i++){
		if(csrRowHostPtr[i] == cutAtIndex)
			return i;
		else if(csrRowHostPtr[i] > cutAtIndex)
			return i;
	}
	return M;
}

/*
 * Before splitting matrix, checks that 1st elt of splitted matrix is at start of line
 */
void ADMMGscon::prepareSplitMatrix(int* csrRowHostPtr, int* valColCutIndex, int* rowCutIndex, int nnz, int M, int ngpus)
{
	int remainingElts = nnz;
	int cutAtIndex = 0;
	for(int i = 0;i<ngpus-1;i++)
	{
		cutAtIndex = cutAtIndex+(remainingElts/(ngpus-i));
		// this gives us the first line which will be in new splitted array
		int lineStartIndex = checkIfIndexIsLineStart(cutAtIndex,csrRowHostPtr,M);
		// this gives us the position in csrVal and csrCol where we will split
		// may not be modified if cutAtIndex was already the start of a line
		cutAtIndex = csrRowHostPtr[lineStartIndex];
		valColCutIndex[i] = cutAtIndex;
		rowCutIndex[i] = lineStartIndex;

		remainingElts -= cutAtIndex;
	}
}

/*
 * Split matrix contained in csrVal, csrCol, csrRow into csrValSplit, csrColSplit, csrRowSplit
 *
 * To split transpose matrix stored in CSC, simply use cscCol for csrRow, cscRow for csrCol and N for M
 */
void ADMMGscon::splitCSRMatrix(const int M, const int nnz, const int* csrRowDevicePtr,
		const int* csrColDevicePtr, const float* csrValDevicePtr,
		int** &csrRowDevicePtrSplit, int** &csrColDevicePtrSplit, float** &csrValDevicePtrSplit,
		int* &subNNZ, int* &subRowNb)
{
	//tic();

	// alloc one array per GPU
	csrRowDevicePtrSplit = (int **)malloc(NGPUS*sizeof(csrRowDevicePtrSplit[0]));
	csrColDevicePtrSplit = (int **)malloc(NGPUS*sizeof(csrColDevicePtrSplit[0]));
	csrValDevicePtrSplit = (float **)malloc(NGPUS*sizeof(csrValDevicePtrSplit[0]));

	int* rowCutIndex = (int *)malloc((NGPUS-1)*sizeof(rowCutIndex[0]));
	int* valColCutIndex = (int *)malloc((NGPUS-1)*sizeof(valColCutIndex[0]));

	int * csrRowHostPtrSmallB = 0;
	cudaHostAlloc((void**) &csrRowHostPtrSmallB, (M+1) * sizeof(csrRowHostPtrSmallB[0]),
			cudaHostAllocDefault);
	cudaMemcpy(csrRowHostPtrSmallB,csrRowDevicePtr,(M+1) * sizeof(csrRowHostPtrSmallB[0]),cudaMemcpyDeviceToHost);

	prepareSplitMatrix(csrRowHostPtrSmallB,valColCutIndex,rowCutIndex,nnz,M,NGPUS);

	cudaPeekAtLastError();
	cudaDeviceSynchronize();

	subNNZ = (int *)malloc(NGPUS*sizeof(subNNZ[0]));
	subRowNb = (int *)malloc(NGPUS*sizeof(subRowNb[0]));

	for(int i = 0;i<NGPUS;i++)
	{
		if(i == 0)
		{
			subNNZ[i] = valColCutIndex[i];
			subRowNb[i] = rowCutIndex[i];
		}
		else if(i == NGPUS-1)
		{
			subNNZ[i] = nnz-valColCutIndex[i-1];
			subRowNb[i] = M-rowCutIndex[i-1];
		}
		else
		{
			subNNZ[i] = valColCutIndex[i]-valColCutIndex[i-1];
			subRowNb[i] = rowCutIndex[i]-rowCutIndex[i-1];
		}
		cudaSetDevice(gpuid[i]);
		cudaMalloc((void**) &csrRowDevicePtrSplit[i], (subRowNb[i] + 1) * sizeof(csrRowDevicePtrSplit[i][0]));
		cudaMalloc((void**) &csrColDevicePtrSplit[i], subNNZ[i] * sizeof(csrColDevicePtrSplit[i][0]));
		cudaMalloc((void**) &csrValDevicePtrSplit[i], subNNZ[i] * sizeof(csrValDevicePtrSplit[i][0]));
	}
	cudaPeekAtLastError();
	cudaDeviceSynchronize();

	int* subNNZDevice[NGPUS]; // is in GPU memory

	for(int i = 0;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		cudaMalloc((void**)&subNNZDevice[i],NGPUS*sizeof(subNNZDevice[0][0]));
		cudaMemcpy(subNNZDevice[i],subNNZ,NGPUS*sizeof(subNNZDevice[0][0]),cudaMemcpyHostToDevice);
	}

	cudaPeekAtLastError();
	cudaDeviceSynchronize();

	// row as col and col as row
	splitMatrixDeviceToDeviceAsync(csrValDevicePtr,csrRowDevicePtr,csrColDevicePtr,
			csrValDevicePtrSplit,csrRowDevicePtrSplit,csrColDevicePtrSplit,
			subNNZ,subNNZDevice,subRowNb,NGPUS,stream);

	cudaPeekAtLastError();
	cudaDeviceSynchronize();

	for(int i = 0;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		cudaDeviceSynchronize();
	}

	for(int i = 0;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		cudaFree(subNNZDevice[i]);
	}

	cudaPeekAtLastError();
	cudaDeviceSynchronize();

	for(int i = 1;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		adjustNewRowArray<<<(subRowNb[i]+255)/256,256>>>(csrRowDevicePtrSplit[i],subRowNb[i],
				csrRowHostPtrSmallB[subRowNb[i-1]]);
	}

	free(valColCutIndex);
	free(rowCutIndex);
	cudaFreeHost(csrRowHostPtrSmallB);

	for(int i = 1;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		cudaDeviceSynchronize();
	}

	//printf("Elapsed time : %lf ms\n", toc() / 1000.0f);

	cudaPeekAtLastError();
	cudaDeviceSynchronize();

	//printf("GPU 0 has %d nnz in %d lines\nGPU 1 has %d nnz in %d lines\n",
			//subNNZ[0],subRowNb[0],subNNZ[1],subRowNb[1]);

	// DO NOT FORGET !!!
	cudaSetDevice(gpuid[0]);
}

/*
 * Launch the copy of splitted matrix device from device (gpu to gpu)
 */
void ADMMGscon::splitMatrixDeviceToDeviceAsync(const float* csrValDevicePtr, const int* csrRowDevicePtr, const int* csrColDevicePtr,
		float** csrValDeviceSplittedPtr, int** csrRowDeviceSplittedPtr, int** csrColDeviceSplittedPtr,int* subNNZ,int** subNNZDevice,
		int* subRowNb, int ngpus, cudaStream_t* stream)
{
	int offset = 0;
	int offsetRow = 0;

	for(int i = 0;i<ngpus;i++)
	{
		cudaSetDevice(gpuid[i]);
		cudaMemcpyAsync(csrValDeviceSplittedPtr[i],csrValDevicePtr+offset,subNNZ[i]*sizeof(csrValDevicePtr[0]),
				cudaMemcpyDeviceToDevice,stream[i]);
		cudaMemcpyAsync(csrColDeviceSplittedPtr[i],csrColDevicePtr+offset,subNNZ[i]*sizeof(csrColDevicePtr[0]),
				cudaMemcpyDeviceToDevice,stream[i]);
		cudaMemcpyAsync(csrRowDeviceSplittedPtr[i],csrRowDevicePtr+offsetRow,subRowNb[i]*sizeof(csrRowDevicePtr[0]),
				cudaMemcpyDeviceToDevice,stream[i]);
		int offsetRowLastElt = subRowNb[i];
		cudaMemcpyAsync(csrRowDeviceSplittedPtr[i]+offsetRowLastElt,subNNZDevice[i]+i,1*sizeof(csrRowDevicePtr[0]),
				cudaMemcpyDeviceToDevice,stream[i]);
		offset += subNNZ[i];
		offsetRow += subRowNb[i];
	}
	cudaSetDevice(gpuid[0]); // set default GPU as active
}

/*
 * Compute the power method on input matrix
 */
int ADMMGscon::pow_method(float* sval, int Ms, int Ns, int nnz,
		int* cscColDevicePtrA, int* cscRowDevicePtrA, float* cscValDevicePtrA,
		int* csrColDevicePtrA, int* csrRowDevicePtrA, float* csrValDevicePtrA,
		cusparseMatDescr_t descr, cusparseHandle_t handle, cublasHandle_t blasHandle)
{
	float dzero = 0.0;
	float done = 1.0;
	/*
	 * sval = pow_method
	 */

	printf("Finding max-norm of B\n");
	tic();
	// create random vector
	float* pMVectorHost = 0;
	pMVectorHost = (float*) malloc(Ms * sizeof(pMVectorHost[0]));
	boost::random::mt19937 gen(time(NULL));
	boost::random::uniform_real_distribution<> dist;
	// generating random values
	for(long int i = 0;i<Ms;i++)
	{
		pMVectorHost[i] = dist(gen);
	}
	float* pMVectorXDevice = 0;
	cudaMalloc((void**) &pMVectorXDevice, Ms * sizeof(pMVectorXDevice[0]));
	cudaMemcpy(pMVectorXDevice,pMVectorHost,Ms*sizeof(pMVectorXDevice[0]),cudaMemcpyHostToDevice);
	free(pMVectorHost);
	float pMNorm = 0.0;
	// pMNorm = norm(x)
	cublasStatus_t blasStatus = cublasSnrm2_v2(blasHandle, Ms, pMVectorXDevice, 1, &pMNorm);
	cudaDeviceSynchronize();
	if (blasStatus != CUBLAS_STATUS_SUCCESS) {
		printf("Error with 2-norm pMNorm\n");
		return 1;
	}
	float pMNormInverse = 1.0/pMNorm;
	// x = x/norm(x)
	blasStatus = cublasSscal_v2(blasHandle,Ms,&pMNormInverse,pMVectorXDevice,1);
	cudaDeviceSynchronize();
	if (blasStatus != CUBLAS_STATUS_SUCCESS) {
		printf("Error with x = x/norm(x)\n");
		return 1;
	}
	const int PMITERS = 100;
	const float TOL = 1e-3;
	// create other temp Vector
	float* pMVectorYDevice = 0;
	cudaMalloc((void**) &pMVectorYDevice, Ns * sizeof(pMVectorYDevice[0]));
	float val = 0.0;
	float init_val = 1.0;
	for(int i = 0;i<PMITERS;i++)
	{
		cusparseStatus_t status = cusparseScsrmv(handle, CUSPARSE_OPERATION_NON_TRANSPOSE, Ns, Ms,
				nnz, &done, descr, cscValDevicePtrA, cscColDevicePtrA,
				cscRowDevicePtrA, &pMVectorXDevice[0], &dzero, &pMVectorYDevice[0]);
		cudaDeviceSynchronize();
		if (status != CUSPARSE_STATUS_SUCCESS) {
			printf("BT*y multiplication failed (power method)\n");
			return 1;
		}
		status = cusparseScsrmv(handle, CUSPARSE_OPERATION_NON_TRANSPOSE, Ms, Ns,
				nnz, &done, descr, csrValDevicePtrA, csrRowDevicePtrA,
				csrColDevicePtrA, &pMVectorYDevice[0], &dzero, &pMVectorXDevice[0]);
		cudaDeviceSynchronize();
		if (status != CUSPARSE_STATUS_SUCCESS) {
			printf("B*x multiplication failed (power method)\n");
			return 1;
		}
		// val = norm(x)
		blasStatus = cublasSnrm2_v2(blasHandle, Ms, pMVectorXDevice, 1, &val);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("Error with 2-norm val (power method)\n");
			return 1;
		}
		// rel_var = abs(val-init_val)/init_val
		float rel_var = std::abs(val-init_val)/init_val;
		//printf("Iter = %d, norm = %f \n",i,val);
		if(rel_var < TOL)
			break;
		init_val = val;
		// x = x/val
		float valInverse = 1/val;
		blasStatus = cublasSscal_v2(blasHandle,Ms,&valInverse,pMVectorXDevice,1);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("Error with x = x/val (power method)\n");
			return 1;
		}
	}

	cudaFree(pMVectorXDevice);
	cudaFree(pMVectorYDevice);

	// val contains value sval²
	*sval = val;
	printf("Elapsed time : %lf ms\n", toc() / 1000.0f);
	printf("sval : %f\n", std::sqrt(*sval));

	return 0;
}

/*
 * Find max of vector
 */
float ADMMGscon::max(float* csrValHostPtr, long size) {
	float tempMax = 0.0;
	for (long i = 0; i < size; i++) {
		float crt = csrValHostPtr[i];
		if (crt > tempMax)
			tempMax = std::abs(crt);
	}
	return tempMax;
}

/*
 * Show the current memory usage of GPU #gpuIndex
 */
cudaError_t ADMMGscon::showGPUMemory(int gpuIndex) {
	// show memory usage of GPU
	size_t free_byte;
	size_t total_byte;
	cudaSetDevice(gpuid[gpuIndex]);
	cudaError_t cudaError = cudaMemGetInfo(&free_byte, &total_byte);

	double free_db = (double) free_byte;
	double total_db = (double) total_byte;
	double used_db = total_db - free_db;

	printf("CSR GPU memory usage: used = %f, free = %f MB, total = %f MB\n",
			used_db / 1024.0 / 1024.0, free_db / 1024.0 / 1024.0,
			total_db / 1024.0 / 1024.0);
	cudaSetDevice(gpuid[0]); // return to default gpu to avoid problems
	return cudaError;
}

/*
 * L2-L1 norm (group sparsity)
 *
 * return fval
 */
float ADMMGscon::l2l1_norm(float* xDevicePtr, float* wDevicePtr, int G, int NG, cublasHandle_t blasHandle)
{
	float fval = 0.0;
	float* normvecDevicePtr = 0;
	cudaMalloc((void**) &normvecDevicePtr, NG * sizeof(normvecDevicePtr[0]));
	cublasStatus_t blasStatus;

	const int MAX_BLOCKS = 65536;

	if(NG > MAX_BLOCKS)
		printf("Warning with number of blocks launched for l2NormVector in l2l1_prox\n");
	l2NormVector<<<NG,256>>>(normvecDevicePtr,xDevicePtr,G,NG);
	cudaDeviceSynchronize();
	// fval = w*normvec
	blasStatus = cublasSdot_v2(blasHandle, NG, wDevicePtr, 1, normvecDevicePtr, 1,&fval);
	cudaDeviceSynchronize();
	if (blasStatus != CUBLAS_STATUS_SUCCESS) {
		printf("l2l1_norm : Error with dot product fval = w*normvec\n");
	}

	cudaFree(normvecDevicePtr);

	return fval;
}

/*
 * L2-L1 proximal operator (group sparsity)
 *
 * we avoid reshaping vector x, because all calcs can be done on the vector
 * cf. Matlab associated code for reshape
 *
 * return 1 if error, 0 else
 */
int ADMMGscon::l2l1_prox(float* xDevicePtr, int sizeX, float* wDevicePtr, int G, int NG,
		float* solDevicePtr, cublasHandle_t blasHandle)
{
	float* normvecDevicePtr = 0;
	cudaMalloc((void**) &normvecDevicePtr, NG * sizeof(normvecDevicePtr[0]));
	// calc all norms, normvec = sqrt(sum(sol.²,1))
	//subtic();
	if(NG > 65536)
		printf("Warning with number of blocks launched for l2NormVector in l2l1_prox\n");
	l2NormVector<<<NG,256>>>(normvecDevicePtr,xDevicePtr,G,NG);

	cudaDeviceSynchronize();

	float* TDevicePtr = 0;
	cudaMalloc((void**) &TDevicePtr, NG * sizeof(TDevicePtr[0]));
	cudaDeviceSynchronize();

	// T = soft(normvec,w'), there is no gammaMu here
	soft<<<(NG+127)/128,128>>>(normvecDevicePtr,NG,wDevicePtr,TDevicePtr);
	cudaDeviceSynchronize();

	float epsilon = std::numeric_limits<float>::epsilon();
	// normvec(normvec == 0) = eps
	zeroToEpsilonVector<<<(NG+127)/128,128>>>(normvecDevicePtr,NG,epsilon);
	cudaDeviceSynchronize();

	// T = T./normvec
	vectorDivision<<<(NG+127)/128,128>>>(TDevicePtr,normvecDevicePtr,TDevicePtr,NG);
	cudaDeviceSynchronize();

	// W = sparse(diag(T)); sol = sol*W; sol = reshape(sol,G*NG,1)
	//subtic();
	l2l1_special_mult<<<((NG*G)+127),128>>>(xDevicePtr, TDevicePtr, solDevicePtr, G, NG);
	cudaDeviceSynchronize();
	//printf("l2l1 special mult : elapsed time : %lf ms\n", subtoc() / 1000.0f);
	cudaFree(normvecDevicePtr);
	cudaFree(TDevicePtr);
	return 0;
}

/*
 * L2-L1 proximal operator (group sparsity) with non-negativity
 *
 * return 1 if error, 0 else
 *
 * output : vDevicePtr, doesn't need to be memset to 0
 */
int ADMMGscon::nnl2l1_prox(float* xDevicePtr, int sizeX, float* gammaMu, float* cDevicePtr, int G, int NG, float tol, int maxiter,
		float* vDevicePtr, cublasHandle_t blasHandle)
{
	// alloc uDevicePtr
	float* wDevicePtr = 0;
	cudaMalloc((void**) &wDevicePtr, NG * sizeof(wDevicePtr[0]));
	// w = c
	cudaMemcpy(wDevicePtr,cDevicePtr,NG*sizeof(wDevicePtr[0]),cudaMemcpyDeviceToDevice);

	// w = c*gamma*mu
	cublasStatus_t blasStatus = cublasSscal_v2(blasHandle,NG,gammaMu,wDevicePtr,1);
	if (blasStatus != CUBLAS_STATUS_SUCCESS) {
		printf("nnl2l1_prox : Error with scal wDevicePtr\n");
		return 1;
	}

	// alloc uDevicePtr
	float* uDevicePtr = 0;
	cudaMalloc((void**) &uDevicePtr, sizeX * sizeof(uDevicePtr[0]));
	// u = zeros(size(x))
	cudaMemset(uDevicePtr,0,sizeX*sizeof(uDevicePtr[0]));

	// v = x
	cudaMemcpy(vDevicePtr,xDevicePtr,sizeX*sizeof(vDevicePtr[0]),cudaMemcpyDeviceToDevice);

	// hard thresholding, v(v<0) = 0;
	negativeToZeroVector<<<(sizeX+255)/256,256>>>(vDevicePtr, sizeX);
	cudaDeviceSynchronize();

	float nvp = 0.0;
	// nvp = norm(v)
	blasStatus = cublasSnrm2_v2(blasHandle, sizeX, vDevicePtr, 1, &nvp);
	cudaDeviceSynchronize();
	if (blasStatus != CUBLAS_STATUS_SUCCESS) {
		printf("nnl2l1_prox : Error with 2-norm nvp\n");
		return 1;
	}
	// alloc temp, size = N
	float* tempDevicePtr = 0;
	cudaMalloc((void**) &tempDevicePtr, sizeX * sizeof(tempDevicePtr[0]));
	float done = 1.0;
	float dminusone = -1.0;
	// will be used in for iterations
	float* vMinTempDevicePtr = 0;
	cudaMalloc((void**) &vMinTempDevicePtr, sizeX * sizeof(vMinTempDevicePtr[0]));
	float* solDevicePtr = 0;
	cudaMalloc((void**) &solDevicePtr, (NG*G) * sizeof(solDevicePtr[0]));
	for(int i = 0;i<maxiter;i++)
	{
		// temp = v
		cudaMemcpy(tempDevicePtr,vDevicePtr,sizeX*sizeof(vDevicePtr[0]),cudaMemcpyDeviceToDevice);

		// temp = v + u
		blasStatus = cublasSaxpy_v2(blasHandle, sizeX, &done, uDevicePtr, 1,
				tempDevicePtr, 1);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("nnl2l1_prox : Error with vector addition B2(xol) - y at iter %d\n",i);
			return 1;
		}

		// sol = l2l1_prox()
		//subtic();
		l2l1_prox(tempDevicePtr,sizeX,wDevicePtr,G,NG,solDevicePtr,blasHandle);
		//printf("l2l1_prox : elapsed time : %lf ms\n", subtoc() / 1000.0f);

		// u = temp
		cudaMemcpy(uDevicePtr,tempDevicePtr,sizeX * sizeof(uDevicePtr[0]),cudaMemcpyDeviceToDevice);
		// u = temp - sol
		blasStatus = cublasSaxpy_v2(blasHandle, sizeX, &dminusone, solDevicePtr, 1,
				uDevicePtr, 1);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("nnl2l1_prox : Error with vector addition temp - sol at iter %d\n",i);
			return 1;
		}

		// temp = v
		cudaMemcpy(tempDevicePtr,vDevicePtr,sizeX * sizeof(tempDevicePtr[0]),cudaMemcpyDeviceToDevice);

		// v = x
		cudaMemcpy(vDevicePtr,xDevicePtr,sizeX*sizeof(vDevicePtr[0]),cudaMemcpyDeviceToDevice);
		// v = x - u, with v already == x
		blasStatus = cublasSaxpy_v2(blasHandle, sizeX, &dminusone, uDevicePtr, 1,
				vDevicePtr, 1);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("nnl2l1_prox : Error with vector addition x - u at iter %d\n",i);
			return 1;
		}

		// v(v<0) = 0
		negativeToZeroVector<<<(sizeX+255)/256,256>>>(vDevicePtr, sizeX);
		cudaDeviceSynchronize();

		float normVMinTemp = 0.0;
		// vMinTempDevicePtr = v
		cudaMemcpy(vMinTempDevicePtr,vDevicePtr,sizeX*sizeof(vMinTempDevicePtr[0]),cudaMemcpyDeviceToDevice);
		// vMinTempDevicePtr = v-temp
		blasStatus = cublasSaxpy_v2(blasHandle, sizeX, &dminusone, tempDevicePtr, 1,
				vMinTempDevicePtr, 1);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("nnl2l1_prox : Error with vector addition v - temp\n");
			return 1;
		}
		// normVMinTemp = norm(v-temp)
		blasStatus = cublasSnrm2_v2(blasHandle, sizeX, vMinTempDevicePtr, 1, &normVMinTemp);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("nnl2l1_prox : Error with 2-norm normVMinTemp\n");
			return 1;
		}

		float rel_dist = 0.0;
		if(nvp > 0.0)
		{
			rel_dist = normVMinTemp/nvp;
		}
		else
		{
			rel_dist = normVMinTemp;
		}
		//printf("Iter %d: rel_dist = %f\n",i,rel_dist);
		if(rel_dist <= tol)
		{
			break;
		}
	}
	cudaFree(tempDevicePtr);
	cudaFree(vMinTempDevicePtr);
	cudaFree(uDevicePtr);
	cudaFree(solDevicePtr);
	cudaFree(wDevicePtr);
	return 0;
}

/*
 * Launch the ADMM algorithm
 */
int ADMMGscon::admm(const int M, const int N, const int NG, const int NGPUS, const float sval, int* subRowNbB,
		int nnz, cusparseMatDescr_t descr, cusparseHandle_t handle, cublasHandle_t blasHandle, float* yDevicePtr,
		cudaStream_t* stream, int** cscColDevicePtrSplittedB, int** cscRowDevicePtrSplittedB, float** cscValDevicePtrSplittedB,
		int** csrColDevicePtrSplittedB, int** csrRowDevicePtrSplittedB, float** csrValDevicePtrSplittedB,
		int* subNNZB, int* subNNZBT, float* cDevicePtr, float** xsolDevicePtr, float* zDevicePtr,
		int* subRowNbBT, int* csrRowDevicePtrB, int* csrColDevicePtrB, float* csrValDevicePtrB,
		const float target_rel_error, const int max_iter, const float gamma, int writeOutputFiles,
		const char* output_xsol_filename)
{
	cusparseStatus_t status;
	cublasStatus_t blasStatus;
	float dzero = 0.0;
	float done = 1.0;
	float dminusone = -1.0;
	/*
	 * init M and N (easy way to compare Matlab code)
	 * size of original problem
	 */
	printf("Init vars, vectors\n");
	tic();

	// size of each group
	const int SG = N/NG;

	/*
	 * init z, size = M
	 */
	if(initZ)
		initArrayKernel<<<(M+255)/256,256>>>(zDevicePtr,M,0.0);

	/*
	 * init r, size = N
	 */

	float * rDevicePtr = 0;
	cudaMalloc((void**) &rDevicePtr, N * sizeof(rDevicePtr[0]));
	initArrayKernel<<<(N+255)/256,256>>>(rDevicePtr,N,0.0);

	/*
	 * init res, size = M
	 */

	float * resDevicePtr = 0;
	cudaMalloc((void**) &resDevicePtr,M * sizeof(resDevicePtr[0]));

	/*
	 * init s, size = M
	 */

	float * sDevicePtr = 0;
	cudaMalloc((void**) &sDevicePtr,M * sizeof(sDevicePtr[0]));

	/*
	 * init temporary vectors
	 */

	float * tempMDevicePtr[NGPUS];
	float * tempNDevicePtr = 0;

	for(int i = 0;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		// vector of size M to store temps values
		cudaMalloc((void**) &tempMDevicePtr[i], M * sizeof(tempMDevicePtr[i][0]));
	}
	cudaSetDevice(gpuid[0]);
	// vector of size N to store temps values
	cudaMalloc((void**) &tempNDevicePtr,N * sizeof(tempNDevicePtr[0]));

	float * halfTempMDevicePtr[NGPUS];
	for(int i = 0;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		cudaMalloc((void**) &halfTempMDevicePtr[i],subRowNbB[i] * sizeof(halfTempMDevicePtr[i][0]));
	}
	float * halfTempNDevicePtr[NGPUS];
	for(int i = 0;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		cudaMalloc((void**) &halfTempNDevicePtr[i],subRowNbBT[i] * sizeof(halfTempNDevicePtr[i][0]));
	}
	cudaSetDevice(gpuid[0]);
	// init algo variables
	float minusmu = -1.0 * mu;
	const float beta = 0.9;

	const int SHOW_LOG = 0;
	const int SAVE_RESULT = 0;

	float fval = 1.0;
	float res3 = 0.0;
	int flag = 0;
	int nbItersFinal = 0;

	printf("Elapsed time : %lf ms\n", toc() / 1000.0f);


	printf("Memory usage\n");
	for(int i = 0;i<NGPUS;i++)
	{
		showGPUMemory(i);
	}


	/**********************************
	 *
	 * admm_gscon
	 *
	 *********************************/

	/*
	 * Initial residual
	 */
	//printf("Initial residual\n");
	//tic();
	// tempM = B*xsol
	status = B2(csrRowDevicePtrB,csrColDevicePtrB,csrValDevicePtrB,xsolDevicePtr[0],tempMDevicePtr[0],
			nnz,M,N,&done,&dzero,handle,descr);
	if (status != CUSPARSE_STATUS_SUCCESS) {
		printf("A*x(t)-y multiplication failed\n");
		return 1;
	}

	// tempM = -y + tempM
	blasStatus = cublasSaxpy_v2(blasHandle, M, &dminusone, yDevicePtr, 1,
			tempMDevicePtr[0], 1);
	cudaDeviceSynchronize();
	if (blasStatus != CUBLAS_STATUS_SUCCESS) {
		printf("Error with vector addition B2(xol) - y\n");
		return 1;
	}
	// res = tempM
	cudaMemcpy(resDevicePtr,tempMDevicePtr[0],(size_t)M*sizeof(resDevicePtr[0]),cudaMemcpyDeviceToDevice);

	//printf("Elapsed time : %lf ms\n", toc() / 1000.0f);

	/*
	 * Main loop
	 */

	printf("\n\nDoing at most %d iters ...\n", max_iter);
	tic();
	for (int i = 0; i < max_iter; i++) {

		// tempM = -z
		cudaMemcpy(tempMDevicePtr[0],zDevicePtr,M*sizeof(tempMDevicePtr[0][0]),cudaMemcpyDeviceToDevice);
		cublasStatus_t blasStatus = cublasSscal_v2(blasHandle,M,&dminusone,tempMDevicePtr[0],1);
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("nnl2l1_prox : Error with scal zDevicePtr\n");
			return 1;
		}

		// tempM = -res+tempM, where tempM has values from -z
		blasStatus = cublasSaxpy_v2(blasHandle, M, &dminusone, resDevicePtr, 1,
				tempMDevicePtr[0], 1);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("Error with vector addition tempM = -z-res at iter %d\n", i);
			return 1;
		}

		// s = hardt(tempM), where tempM has values of -z-res
		hardt<<<(M+255)/256,256>>>(tempMDevicePtr[0],sDevicePtr,M);
		cudaPeekAtLastError();
		cudaDeviceSynchronize();

		// tempM = res
		cudaMemcpy(tempMDevicePtr[0], resDevicePtr,
				(size_t) (M * sizeof(tempMDevicePtr[0][0])),
				cudaMemcpyDeviceToDevice);

		// tempM = z + tempM, where tempM has values from res
		blasStatus = cublasSaxpy_v2(blasHandle, M, &done, zDevicePtr, 1,
				tempMDevicePtr[0], 1);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("Error with vector addition tempM = z + res at iter %d\n", i);
			return 1;
		}

		// tempM = s + tempM, where tempM has values from z+res
		blasStatus = cublasSaxpy_v2(blasHandle, M, &done, sDevicePtr, 1,
				tempMDevicePtr[0], 1);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("Error with vector addition tempM = z + res + s at iter %d\n", i);
			return 1;
		}

		// send tempM to all GPUs
		//subtic();
		for(int j = 1;j<NGPUS;j++)
		{
			cudaSetDevice(gpuid[j]);
			cudaMemcpyAsync(tempMDevicePtr[j],tempMDevicePtr[0],M*sizeof(tempMDevicePtr[0][0]),cudaMemcpyDeviceToDevice,stream[j]);
		}
		for(int j = 0;j<NGPUS;j++)
		{
			cudaSetDevice(gpuid[j]);
			// tempM = B*xsol
			if(j>0)
				cudaStreamSynchronize(stream[j]);
			// tempN = AT * tempM, where tempM = (z + res + s)
			status = B2t(cscColDevicePtrSplittedB[j],cscRowDevicePtrSplittedB[j],cscValDevicePtrSplittedB[j],tempMDevicePtr[j],halfTempNDevicePtr[j],
					subNNZBT[j],subRowNbBT[j],M,&done,&dzero,handle,descr);
			if (status != CUSPARSE_STATUS_SUCCESS) {
				printf("A*x(t)-y multiplication failed at iter %d\n",i);
				return 1;
			}
		}
		for(int j = 0;j<NGPUS;j++)
		{
			cudaSetDevice(gpuid[j]);
			cudaDeviceSynchronize();
		}

		// tempNDevicePtr has to be merged
		cudaSetDevice(gpuid[0]);
		int copiedItems = 0;
		for(int j = 0;j<NGPUS;j++)
		{
			cudaMemcpyAsync(tempNDevicePtr+copiedItems,halfTempNDevicePtr[j],subRowNbBT[j]*sizeof(tempNDevicePtr[0]),cudaMemcpyDeviceToDevice,stream[j]);
			copiedItems += subRowNbBT[j];
		}
		for(int j = 0;j<NGPUS;j++)
		{
			cudaSetDevice(gpuid[j]);
			cudaStreamSynchronize(stream[j]);
		}
		cudaSetDevice(gpuid[0]);
		//printf("B2t Elapsed time : %lf ms\n", subtoc() / 1000.0f);


		// r = tempN
		cudaMemcpy(rDevicePtr,tempNDevicePtr,(size_t)N*sizeof(rDevicePtr[0]),cudaMemcpyDeviceToDevice);

		// tempN = xsol
		cudaMemcpy(tempNDevicePtr, xsolDevicePtr[0],
				(size_t) (N * sizeof(tempNDevicePtr[0])),
				cudaMemcpyDeviceToDevice);

		// tempN = -mu*r + tempN, where tempN input has values from xsol
		blasStatus = cublasSaxpy_v2(blasHandle, N, &minusmu, rDevicePtr, 1,
				tempNDevicePtr, 1);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("Error with vector addition tempN1 = -mu*r + tempN at iter %d\n", i);
			return 1;
		}

		// r = tempN
		cudaMemcpy(rDevicePtr, tempNDevicePtr,	(size_t) (N * sizeof(rDevicePtr[0])),
				cudaMemcpyDeviceToDevice);

		// final output is written to x from nnl2l1
		float nnl2l1_prox_tol = 0.001;

		// alloc uDevicePtr
		float* vDevicePtr = 0;
		cudaMalloc((void**) &vDevicePtr, N * sizeof(vDevicePtr[0]));
		float gammaMu = gamma*mu;

		// nnl2l1_prox
		//subtic();
		nnl2l1_prox(rDevicePtr,N,&gammaMu,cDevicePtr,SG,NG,nnl2l1_prox_tol,nnl2l1_prox_maxiter,vDevicePtr,blasHandle);
		//printf("nnl2l1 elapsed time : %lf ms\n", subtoc() / 1000.0f);
		cudaMemcpy(xsolDevicePtr[0],vDevicePtr,N*sizeof(xsolDevicePtr[0][0]),cudaMemcpyDeviceToDevice);
		cudaFree(vDevicePtr);

		// cuSparseXcsrmv modifies y, so use tempM instead of y to keep y data safe
		cudaMemcpy(tempMDevicePtr[0], yDevicePtr,
				(size_t) (M * sizeof(tempMDevicePtr[0][0])),
				cudaMemcpyDeviceToDevice);

		//subtic();
		// do B*xsol splitted acrossed multiple GPUs
		for(int j = 1;j<NGPUS;j++)
		{
			cudaSetDevice(gpuid[j]);
			cudaMemcpyAsync(xsolDevicePtr[j],xsolDevicePtr[0],N*sizeof(xsolDevicePtr[0][0]),cudaMemcpyDeviceToDevice,stream[j]);
		}
		for(int j = 0;j<NGPUS;j++)
		{
			cudaSetDevice(gpuid[j]);
			// tempM = B*xsol
			if(j>0)
				cudaStreamSynchronize(stream[j]);
			//subtic();
			status = B2(csrRowDevicePtrSplittedB[j],csrColDevicePtrSplittedB[j],csrValDevicePtrSplittedB[j],xsolDevicePtr[j],halfTempMDevicePtr[j],
					subNNZB[j],subRowNbB[j],N,&done,&dzero,handle,descr);
			//cudaDeviceSynchronize();
			//printf("B2 Elapsed time : %lf ms\n", subtoc() / 1000.0f);
			if (status != CUSPARSE_STATUS_SUCCESS) {
				printf("B*xsol multiplication failed\n");
				return 1;
			}
		}
		for(int j = 0;j<NGPUS;j++)
		{
			cudaSetDevice(gpuid[j]);
			cudaDeviceSynchronize();
		}
		// tempMDevicePtr has to be merged
		cudaSetDevice(gpuid[0]);
		copiedItems = 0;
		for(int j = 0;j<NGPUS;j++)
		{
			cudaMemcpyAsync(tempMDevicePtr[0]+copiedItems,halfTempMDevicePtr[j],subRowNbB[j]*sizeof(tempMDevicePtr[0][0]),cudaMemcpyDeviceToDevice,stream[j]);
			copiedItems += subRowNbB[j];
		}
		for(int j = 0;j<NGPUS;j++)
		{
			cudaSetDevice(gpuid[j]);
			cudaStreamSynchronize(stream[j]);
		}
		cudaSetDevice(gpuid[0]);
		//printf("B2 Elapsed time : %lf ms\n", subtoc() / 1000.0f);

		// tempM = -y + tempM
		blasStatus = cublasSaxpy_v2(blasHandle, M, &dminusone, yDevicePtr, 1,
				tempMDevicePtr[0], 1);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("Error with vector addition B2(xol) - y at iter %d\n",i);
			return 1;
		}

		// res = tempM, update res with tempM data (tempM = B*xsol-y)
		cudaMemcpy(resDevicePtr, tempMDevicePtr[0],(size_t) (M * sizeof(tempMDevicePtr[0][0])),
				cudaMemcpyDeviceToDevice);

		// Lagrange multipliers update

		// tempM = res
		cudaMemcpy(tempMDevicePtr[0], resDevicePtr,
				(size_t) (M * sizeof(tempMDevicePtr[0][0])),
				cudaMemcpyDeviceToDevice);

		// tempM = s + tempM, where tempM has values from res
		blasStatus = cublasSaxpy_v2(blasHandle, M, &done, sDevicePtr, 1,
				tempMDevicePtr[0], 1);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("Error with vector addition tempM = z + res at iter %d\n", i);
			return 1;
		}

		// z = z + beta*(res+s)
		blasStatus = cublasSaxpy_v2(blasHandle, M, &beta, tempMDevicePtr[0], 1,
				zDevicePtr, 1);
		cudaDeviceSynchronize();
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("Error with vector addition z=z+beta*res at iter %d\n", i);
			return 1;
		}


		/*
		 * Feasibility
		 */

		// init vec, size = M
		float* vecDevicePtr = 0;
		cudaMalloc((void**) &vecDevicePtr, M * sizeof(vecDevicePtr[0]));

		// Feasibility, vec = res
		cudaMemcpy(vecDevicePtr, resDevicePtr,
				(size_t) (M * sizeof(resDevicePtr[0])),
				cudaMemcpyDeviceToDevice);
		negativeToZeroVector<<<(M + 255) / 256, 256>>>(vecDevicePtr, M);
		cudaDeviceSynchronize();
		float res1 = 0.0;

		// res1 = 2-norm(vec)
		blasStatus = cublasSnrm2_v2(blasHandle, M, vecDevicePtr, 1, &res1);
		if (blasStatus != CUBLAS_STATUS_SUCCESS) {
			printf("Error with 2-norm res1 at iter %d\n", i);
			return 1;
		}

		// res3 = norm(vec./abs(y2),'inf')
		vectorDivision<<<(M + 255) / 256, 256>>>(vecDevicePtr, yDevicePtr,
				tempMDevicePtr[0], M);
		cudaDeviceSynchronize();
		float* tempMHostPtr = 0;
		tempMHostPtr = (float *) malloc(M*sizeof(tempMHostPtr[0]));
		cudaMemcpy(tempMHostPtr,tempMDevicePtr[0],(size_t)M*sizeof(tempMHostPtr[0]),cudaMemcpyDeviceToHost);
		res3 = max(tempMHostPtr, M);
		cudaFree(vecDevicePtr);
		free(tempMHostPtr);

		// Objective function
		float rel_fval = 0.0;
		float prev_fval = fval;
		//subtic();
		fval = l2l1_norm(xsolDevicePtr[0],cDevicePtr,SG,NG, blasHandle);
		//printf("l2l1_norm elapsed time : %lf ms\n", subtoc() / 1000.0f);

		if (prev_fval == 0) {
			rel_fval = std::abs(fval);
		} else {
			rel_fval = std::abs(fval - prev_fval) / std::abs(prev_fval);
		}

		// Total error
		float rel_error = res3 + rel_fval;

		// Log
		if(SHOW_LOG)
		{
			printf("Iter %d\n",i);

			printf("L2 error. Lin. con.: %f, fval: %f\n",res1,fval);
			printf("Max relative error:\n");
			printf("Lin. con.: %f, obj. func.: %f, Total : %f\n",res3,rel_fval,rel_error);

			printf("\n\n");
		}

		// Check stopping criteria
		if (rel_error <= target_rel_error) {
			flag = 1;
			nbItersFinal = i;
			break;
		}
	}
	printf("Elapsed time : %lf ms\n\n\n", toc() / 1000.0f);

	/*
	 * Final log
	 */
	//if(flag)
	//{
		printf("Solution found in %d iters\n",nbItersFinal);
	//}
	//else
	//{
		//printf("No feasible solution found\n");
	//}

	printf("Objective function: %f, Relative feasibility error: %f\n",fval,res3);


	if(SAVE_RESULT)
	{
		float* xsolHostPtr = 0;
		xsolHostPtr = (float*) malloc(N*sizeof(xsolHostPtr[0]));
		cudaMemcpy(xsolHostPtr,xsolDevicePtr[0],N*sizeof(xsolHostPtr[0]),cudaMemcpyDeviceToHost);
		std::cout << "Writing xsol to file" << std::endl;
		FILE *f = fopen(output_xsol_filename, "w");
		if (f == NULL)
		{
			std::cout << "Error opening file!" << std::endl;
			exit(1);
		}
		for(int i = 0;i<N;i++)
		{
			fprintf(f, "%.16e\n", xsolHostPtr[i]);
		}
		free(xsolHostPtr);
		fclose(f);
	}

	if (rDevicePtr)
		cudaFree(rDevicePtr);
	if (resDevicePtr)
		cudaFree(resDevicePtr);
	if (sDevicePtr)
		cudaFree(sDevicePtr);
	for(int i = 0;i<NGPUS;i++)
	{
		cudaSetDevice(gpuid[i]);
		cudaFree(halfTempMDevicePtr[i]);
		cudaFree(halfTempNDevicePtr[i]);
		cudaFree(tempMDevicePtr[i]);
	}
	cudaSetDevice(gpuid[0]);
	return 0;
}

