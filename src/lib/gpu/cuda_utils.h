/*
 *  cuda_utils.h
 *
 *  Created on: Jan 17, 2015
 *  Author: Marco Lourenço
 *  Company: HEIA-FR
 */

#ifndef CUDA_UTILS_H_
#define CUDA_UTILS_H_

/*
 * Useful to debug CUDA methods
 */

static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
    if (err != cudaSuccess) {
        printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
                file, line );
        exit( EXIT_FAILURE );
    }
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))


#endif /* CUDA_UTILS_H_ */
