/*
 *  time_utils.h
 *
 *  Created on: Jan 4, 2015
 *  Author: Marco Lourenço
 *  Company: HEIA-FR
 */

#ifndef TIME_UTILS_H_
#define TIME_UTILS_H_

#include <time.h>

/*
 * Useful timing methods
 */

struct timespec globalInitTicToc;
struct timespec globalAfterTicToc;
struct timespec initTicToc;
struct timespec afterTicToc;
struct timespec subinitTicToc;
struct timespec subafterTicToc;

void globaltic() {
	clock_gettime(CLOCK_MONOTONIC, &globalInitTicToc);
}

void tic() {
	clock_gettime(CLOCK_MONOTONIC, &initTicToc);
}

void subtic() {
	clock_gettime(CLOCK_MONOTONIC, &subinitTicToc);
}

double globaltoc() {
	clock_gettime(CLOCK_MONOTONIC, &globalAfterTicToc);
	double us = (globalAfterTicToc.tv_sec - globalInitTicToc.tv_sec) * 1000000.0f;
	return us + (globalAfterTicToc.tv_nsec - globalInitTicToc.tv_nsec) / 1000.0f;
}

double toc() {
	clock_gettime(CLOCK_MONOTONIC, &afterTicToc);
	double us = (afterTicToc.tv_sec - initTicToc.tv_sec) * 1000000.0f;
	return us + (afterTicToc.tv_nsec - initTicToc.tv_nsec) / 1000.0f;
}

double subtoc() {
	clock_gettime(CLOCK_MONOTONIC, &subafterTicToc);
	double us = (subafterTicToc.tv_sec - subinitTicToc.tv_sec) * 1000000.0f;
	return us + (subafterTicToc.tv_nsec - subinitTicToc.tv_nsec) / 1000.0f;
}

#endif /* TIME_UTILS_H_ */
