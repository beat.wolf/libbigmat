/*
 *  ADMMGscon.h
 *
 *  Created on: Jan 4, 2015
 *  Author: Marco Lourenço, Beat Wolf
 *  Company: HEIA-FR
 */

#ifndef ADMMGSCON_H_
#define ADMMGSCON_H_

#include <cusparse.h>
#include <cublas.h>

#include "Optimizer.h"

class ADMMGscon: public Optimizer {
public:
	ADMMGscon(const int NGPUS);
	~ADMMGscon();

	void setMatrixAParameters(AMatrixParameters &parameters);
	int generateA();
	void generateDosemap();
	//void setLabelmap(int size, int *labelMap);
	void setOptimizationParameters(OptimizationParameters &parameters);
	int optimize();
	int getXSolSize();
	OPTIMIZATION_PRECISION* getXSol();
	int getDosemapSize();
	SOLUTION_PRECISION* getDosemap();
private:

	/*
	 * Private attributes
	 */
	int gpuid[64]; // we want to find the first GPU's that can support P2P
	int NGPUS;
	long numCPU;

	AMatrixParameters matrixParameters;
	OptimizationParameters optiParameters;

	int * csrColHostPtrA;
	int * csrRowHostPtrA;
	float * csrValHostPtrA;

	int * csrRowDevicePtrA;
	int * csrColDevicePtrA;
	float * csrValDevicePtrA;

	int * csrColHostPtrB;
	int * csrRowHostPtrB;
	float * csrValHostPtrB;

	int * csrColDevicePtrB;
	int * csrRowDevicePtrB;
	float * csrValDevicePtrB;

	int * csrRowDevicePtrSmallB;
	int * csrColDevicePtrSmallB;
	float * csrValDevicePtrSmallB;

	int * cscRowDevicePtrSmallB;
	int * cscColDevicePtrSmallB;
	float * cscValDevicePtrSmallB;

	int** csrRowDevicePtrSplittedSmallB;
	int** csrColDevicePtrSplittedSmallB;
	float** csrValDevicePtrSplittedSmallB;

	int** cscRowDevicePtrSplitSB;
	int** cscColDevicePtrSplitSB;
	float** cscValDevicePtrSplitSB;

	float** xsolDevicePtr;
	float** smallXsolDevicePtr;

	float* cDevicePtr;
	float* zDevicePtr;

	int Ns;
	int Ms;
	int Ms1;
	int nnz;
	int NG;
	int NB;

	int* nbOfNewLines;

	OPTIMIZATION_PRECISION* xsolHostFinal;
	int xsolHostFinalSize;
	SOLUTION_PRECISION* dosemapHostFinal;

	int* labelMapHostPtr;
	int labelMapSize;

	bool initZ;
	float mu;

	float relativeErrorStage1;
	float maxIterationsStage1;
	float relativeErrorStage2;
	float maxIterationsStage2;

	int nnl2l1_prox_maxiter;

	// cuda related vars
	cudaStream_t* stream;
	cusparseStatus_t status;
	cusparseHandle_t handle;
	cusparseMatDescr_t descr;
	cublasStatus_t blasStatus;
	cublasHandle_t blasHandle;
	cudaStream_t copyAStream;

	// resizeMatrix
	int newNNZ;
	int Ns1;
	int NG1;
	int* normvecIndexHostPtr;

	//createYFromLabelMap
	float * yDevicePtr;
	int * yLabelMapDevicePtr;
	int* lowerBoundRegionIDs;
	int lowerBoundRegions;
	int** linesIDHostPtr;

	/*
	 * Private methods
	 */
	int init(); // mainly used to init libraries like cusparse (if needed)
	int checkIfIndexIsLineStart(int cutAtIndex,int* csrRowHostPtr,int M);
	void prepareSplitMatrix(int* csrRowHostPtr, int* valColCutIndex, int* rowCutIndex, int nnz, int M, int ngpus);
	void splitCSRMatrix(const int M, const int nnz, const int* csrRowDevicePtr,
			const int* csrColDevicePtr, const float* csrValDevicePtr,
			int** &csrRowDevicePtrSplit, int** &csrColDevicePtrSplit, float** &csrValDevicePtrSplit,
			int* &subNNZ, int* &subRowNb);
	void splitMatrixDeviceToDeviceAsync(const float* csrValDevicePtr, const int* csrRowDevicePtr, const int* csrColDevicePtr,
			float** csrValDeviceSplittedPtr, int** csrRowDeviceSplittedPtr, int** csrColDeviceSplittedPtr,int* subNNZ,int** subNNZDevice,
			int* subRowNb, int ngpus, cudaStream_t* stream);
	int pow_method(float* sval, int Ms, int Ns, int nnz,int* cscColDevicePtrA, int* cscRowDevicePtrA, float* cscValDevicePtrA,
			int* csrColDevicePtrA, int* csrRowDevicePtrA, float* csrValDevicePtrA,cusparseMatDescr_t descr,
			cusparseHandle_t handle, cublasHandle_t blasHandle);
	float max(float* csrValHostPtr, long size);
	cudaError_t showGPUMemory(int gpuIndex);
	float l2l1_norm(float* xDevicePtr, float* wDevicePtr, int G, int NG, cublasHandle_t blasHandle);
	int l2l1_prox(float* xDevicePtr, int sizeX, float* wDevicePtr, int G, int NG,
			float* solDevicePtr, cublasHandle_t blasHandle);
	int nnl2l1_prox(float* xDevicePtr, int sizeX, float* gammaMu, float* cDevicePtr, int G, int NG, float tol, int maxiter,
			float* vDevicePtr, cublasHandle_t blasHandle);
	int admm(const int M, const int N, const int NG, const int NGPUS, const float sval, int* subRowNbA,
			int nnz, cusparseMatDescr_t descr, cusparseHandle_t handle, cublasHandle_t blasHandle, float* yDevicePtr,
			cudaStream_t* stream, int** cscColDevicePtrSplittedA, int** cscRowDevicePtrSplittedA, float** cscValDevicePtrSplittedA,
			int** csrColDevicePtrSplittedA, int** csrRowDevicePtrSplittedA, float** csrValDevicePtrSplittedA,
			int* subNNZA, int* subNNZAT, float* cDevicePtr, float** xsolDevicePtr, float* zDevicePtr,
			int* subRowNbAT, int* csrRowDevicePtrA, int* csrColDevicePtrA, float* csrValDevicePtrA,
			const float target_rel_error, const int max_iter, const float gamma, int writeOutputFiles,
			const char* output_xsol_filename);
	void appendLinesToMatrix(int* &csrRowDevicePtrA,int* &csrColDevicePtrA,float* &csrValDevicePtrA,
			int* &csrRowDevicePtrB,int* &csrColDevicePtrB,float* &csrValDevicePtrB,
			int* linesIDHostPtr,float augmentedSign, int &nbOfNewLines);
	int nbOfEltOfCsrLine(int* csrRowDevicePtr, int lineID);
	void resizeMatrix();
	void createYFromLabelmap();
	int clean();
	void augmentY();
	void createB();

	/*
	 * Inline methods
	 */
	bool IsGPUCapableP2P(cudaDeviceProp *pProp) const { return (bool)(pProp->major >= 2); }
	/*
	 * corresponds to B2 macro in matlab
	 *
	 * resultDevicePtr must be of size M and not useful for other methods (values will be erased)
	 *
	 * resultDevicePtr will contain result of operation : B*x(1:N) + x(N+1:N1)
	 */
	cusparseStatus_t B2(int* csrRowDevicePtrB, int* csrColDevicePtrB, float* csrValDevicePtrB,
			float* xDevicePtr, float* resultDevicePtr, int nnz, int M, int N, float* done, float* dzero,
			cusparseHandle_t handle, cusparseMatDescr_t descr) const
	{
		//subtic();
		cusparseStatus_t status = cusparseScsrmv(handle, CUSPARSE_OPERATION_NON_TRANSPOSE, M, N,
				nnz, done, descr, csrValDevicePtrB, csrRowDevicePtrB,
				csrColDevicePtrB, &xDevicePtr[0], dzero, &resultDevicePtr[0]);
		//cudaDeviceSynchronize();
		//printf("B2 Elapsed time : %lf ms\n", subtoc() / 1000.0f);
		return status;
	}

	/*
	 * corresponds to B2t macro in matlab
	 *
	 * resultDevicePtr must be of size N1 and not useful for other methods (values will be erased)
	 *
	 * resultDevicePtr will contain result of operation : [B'*x;x]
	 */
	cusparseStatus_t B2t(int* csrRowDevicePtrBT, int* csrColDevicePtrBT, float* csrValDevicePtrBT,
			float* xDevicePtr, float* resultDevicePtr, int nnz, int M, int N, float* done,
			float* dzero, cusparseHandle_t handle, cusparseMatDescr_t descr) const
	{
		//printf("Doing B2T\n");
		//subtic();
		cusparseStatus_t status = cusparseScsrmv(handle, CUSPARSE_OPERATION_NON_TRANSPOSE, M, N,
				nnz, done, descr, csrValDevicePtrBT, csrRowDevicePtrBT,
				csrColDevicePtrBT, &xDevicePtr[0], dzero, &resultDevicePtr[0]);
		//cudaDeviceSynchronize();
		//printf("B2t Elapsed time : %lf ms\n", subtoc() / 1000.0f);
		return status;
	}
};

#endif /* ADMMGSCON_H_ */
