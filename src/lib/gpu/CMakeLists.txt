find_package(CUDA)

if( CUDA_FOUND )
	message(STATUS "Add GPU optimization")

	set(FILES
	${CMAKE_CURRENT_SOURCE_DIR}/time_utils.h
	${CMAKE_CURRENT_SOURCE_DIR}/cuda_utils.h
	${CMAKE_CURRENT_SOURCE_DIR}/ADMMGscon.h
	${CMAKE_CURRENT_SOURCE_DIR}/ADMMGscon.cu
	)

	message(STATUS ${FILES})

	set(CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS};-gencode arch=compute_35,code=compute_35)
	set(CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS};-Xcompiler;-fpic)

	cuda_add_library(GPUCode ${FILES})
	cuda_include_directories( GPUCode ${BigMatOptimizer_SOURCE_DIR}/src/lib/ )
	
	CUDA_ADD_CUBLAS_TO_TARGET(GPUCode)

	#Newer versions have CUDA_ADD_CUSPARSE_TO_TARGET
	find_cuda_helper_libs(cusparse)
	target_link_libraries(GPUCode ${CUDA_cusparse_LIBRARY})
	target_link_libraries(GPUCode GPUOptimizer)

	target_include_directories (GPUCode PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
	target_include_directories (GPUCode PUBLIC ${CUDA_INCLUDE_DIRS})
	target_include_directories (GPUCode PUBLIC ${BigMatOptimizer_SOURCE_DIR}/src/lib/)
	
	#include_directories(${CUDA_INCLUDE_DIRS})
	set (BUILD_SHARED_LIBS FALSE)
	set_property(TARGET GPUCode PROPERTY POSITION_INDEPENDENT_CODE TRUE)

endif()
