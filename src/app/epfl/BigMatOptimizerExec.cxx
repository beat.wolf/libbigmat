#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>

#include "Optimizer.h"
//#include "DummyOptimizer.h"
#include "gpu/ADMMGscon.h"

/**
 * @brief Save a vector of elements in a text file. One element by row
 * @param filename
 * @param vectorSize
 * @param vector
 * @return 0, if no runtime error
 */
template <class T>
int saveVectorToFile(std::string filename, int vectorSize, T vector[])
{
    std::ofstream output_file;
    output_file.open (filename.c_str());

    for (int i = 0; i < vectorSize; ++i)
    {
        output_file<< "" << vector[i];
        if(i<vectorSize-1)
           output_file<< "\n";
    }
    output_file.close();

    return 0;
}

/**
 * @brief Reads labelmap vector file
 * @param filename
 * @param labelmapVector
 * @return The labelmap as a vector
 */
void readLabelmapVectorFile(std::string filename, std::vector<int>& labelmapVector)
{
    std::ifstream infile(filename.c_str());
    int label=0;
    // Reads a label by line
    while (infile >> label) {               
        labelmapVector.push_back(label);
    }
}

/**
 * @brief Reads doses file
 * @param filename
 * @param doses
 * @param ids
 * @return The number of structures that only considers the max constraint
 */
int readDosesFile(std::string filename, std::vector<float>& dosesMaxConstraints, std::vector<int>& ids)
{
    std::ifstream infile(filename.c_str());
    float totalDoseConstraints=0, numMaxConstraints=0;
    // Reads in the first line of the file the total number of constraints
    infile >> totalDoseConstraints;

    // Reads in the second line, the number of constraints only with a maximum dose
    infile >> numMaxConstraints;
    for (int i = 0; i < totalDoseConstraints; ++i) {
        int idMaxConstraint=0;
        float minConstraint=0;
        float maxConstraint=0;
        if(i<numMaxConstraints)
            // Reads in the next lines, the ID, the mimimun and the maximum constraint for the rest
            infile >> idMaxConstraint >> maxConstraint;
        else
        {
            // Reads in the next lines, the ID and the maximum constraint for the ones with only with a maximum dose
            infile >> idMaxConstraint >> minConstraint >> maxConstraint;
        }
        ids.push_back(idMaxConstraint);
        dosesMaxConstraints.push_back(maxConstraint);
    }
    return numMaxConstraints;
}



/**
 * @brief Main method to run optimization code with a set of predefined arguments and save a solution vector as a text file
 * @param argc
 * @param argv outFolder AMatrixFileName solutionVectorFileName numBeams numBeamletsByBeamX numBeamletsByBeamY
 * @return 0, if no runtime error
 */
int main ( int argc, char *argv[] )
{
  if ( argc != 11 )
  {
    // outFolder
    // AMatrixFileName
    // numBeams
    // numBeamletsByBeam
    std::cout<<"-- ERROR ------------------ "<<argc<<std::endl;
    std::cout<<"Check the arguments"<<std::endl;
    std::cout<<"Usage: "<< argv[0] << " isPrecalculated tempBigmatFolder caseName dosesFileName labelmapFilename AMatrixFilename solutionVectorFileName numBeams numBeamletsByBeamX numBeamletsByBeamY" << std::endl;
    std::cout<<"Example: "<< argv[0] << "1 ~/.bigmat/ case2 case2_doses.csv case2_labelmapVector.dat case2_AMatrix.dat case2_solutionVector.txt 115 20 20" << std::endl;

  }
  else
  {
    struct stat info;

    bool isDirOK=false;
    if( stat( argv[2], &info ) != 0 )
        printf( "cannot access %s\n", argv[2] );
    else if( info.st_mode & S_IFDIR )  // To chek in windows
    {
        printf( "%s is a directory\n", argv[2] );
        isDirOK=true;
    }
    else
        printf( "%s is not directory\n", argv[2] );

    if (!isDirOK)
        return 0;

    std::cout<<"Parameters------------"<<std::endl;

    std::string absoluteDosesFileName = std::string(argv[2]).append("/");
    absoluteDosesFileName.append(argv[4]);
    std::cout<<"Absolute doses filename:"<<absoluteDosesFileName<<std::endl;

    std::string absoluteLabelmapFileName = std::string(argv[2]).append("/");
    absoluteLabelmapFileName.append(argv[5]);
    std::cout<<"Absolute labelmap vector filename:"<<absoluteLabelmapFileName<<std::endl;

    std::string absoluteAMatrixFileName = std::string(argv[2]).append("/");
    absoluteAMatrixFileName.append(argv[6]);
    std::cout<<"Absolute A Matrix filename:"<<absoluteAMatrixFileName<<std::endl;

    std::string absoluteSolutionVectorFileName = std::string(argv[2]).append("/");
    absoluteSolutionVectorFileName.append(argv[7]);
    std::cout<<"Absolute solution vector filename:"<<absoluteSolutionVectorFileName<<std::endl;

    std::cout<<"Number of beams:"<<argv[8]<<std::endl;
    std::cout<<"Number of beamlets by beam:"<<argv[9]<<"x"<<argv[10]<<std::endl;

    std::cout<<"Launching optimization ------------"<<std::endl;
    int nbGPUs = 2;
    Optimizer *lib = new ADMMGscon(nbGPUs);
	
	AMatrixParameters matrixA;
   // matrixA.setAMatrix(absoluteAMatrixFileName);
    //matrixA.setMlcWidth(20);
    //matrixA.setBeamletWidth(5);
	
	lib->setMatrixAParameters(matrixA);
	OptimizationParameters optimization;

    // ONLY NEEDED FOR THE SAMPLE CASE
    optimization.setBeamCount(57);
    optimization.setBeamWidth(20);
    optimization.setBeamHeight(20);

    optimization.setMaxIterationsStage1(3000);
    optimization.setMaxIterationsStage2(15000);
    optimization.setRelativeErrorStage1(0.1);
    optimization.setRelativeErrorStage2(0.02);
    optimization.setGammaStage1(1.0);
    optimization.setGammaStage2(1.0);

    std::vector<int> labelmapVector;
    readLabelmapVectorFile(absoluteLabelmapFileName,labelmapVector);
   // lib->setLabelmap(labelmapVector.size(), labelmapVector.data());

    std::vector<float> doses;
    std::vector<int> ids;
    int numMaxConst = readDosesFile(absoluteDosesFileName, doses, ids);

    ////// Only performed for the last dose set in the A matrix process, i.e. some points in the body with a fixed 45 Gy dose!
    int maxId =-1;
    for (int i = 0; i < ids.size(); ++i) {
        if(ids[i]>maxId)
            maxId=ids[i];
    }
    ids.push_back(maxId+1);
    doses.push_back(45.0);

    //optimization.setRegions(ids.size(), doses.data(), ids.data(), numMaxConst);

	lib->setOptimizationParameters(optimization);	
	lib->generateA();
	lib->optimize();
	
    int size = lib->getXSolSize();
    float* vect = lib->getXSol();

    std::cout<<"Optimization finished ------------"<<std::endl;
    saveVectorToFile(absoluteSolutionVectorFileName,size,vect);
    std::cout<<"Solution vector saved:" << absoluteSolutionVectorFileName <<std::endl;
    delete vect;


  }
  return 0;




}
