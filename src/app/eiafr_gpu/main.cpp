#include <iostream>

#include "Optimizer.h"
#include "cpu/MatrixGenerator.h"
#include "gpu/ADMMGscon.h"

#include <fstream>

const std::string DATAFOLDER("/home/localuser/Documents/dev/libbigmat/data/");

SOURCE_PRECISION** loadBinary(std::string &file, int &cols, int &rows){
	std::ifstream infile;

	infile.open (file.c_str(), std::ios::in | std::ios::binary);

	infile.read((char *) &rows, sizeof(rows));
	infile.read((char *) &cols, sizeof(cols));

	std::cout << cols << " " << rows << std::endl;

	SOURCE_PRECISION ** table = new SOURCE_PRECISION*[rows];

	for(int y = 0; y < rows; y++){
		table[y] = new SOURCE_PRECISION[cols];
	}

	double temp;

	for(int x = 0; x < cols; x++){
		for(int y = 0; y < rows; y++){
			infile.read((char *) &temp, sizeof(temp));
			table[y][x] = temp;
		}
	}

	infile.close();

	return table;
}

SOURCE_PRECISION* loadBinaryVector(std::string &file, int &rows){
	std::ifstream infile;

	infile.open (file.c_str(), std::ios::in | std::ios::binary);

	infile.read((char *) &rows, sizeof(rows));

	SOURCE_PRECISION * table = new SOURCE_PRECISION[rows];

	double temp;
	for(int y = 0; y < rows; y++){
		infile.read((char *) &temp, sizeof(temp));
		table[y] = temp;
	}

	infile.close();

	return table;
}

int* readLabelMap(std::string &file, int *labelmapSize, SOURCE_PRECISION *offset, SOURCE_PRECISION *pixelDimensions){

	std::ifstream infile;

	infile.open (file.c_str(), std::ios::in | std::ios::binary);

	int32_t width, height, depth;
	infile.read((char *) &height, sizeof(height));
	infile.read((char *) &width, sizeof(width));
	infile.read((char *) &depth, sizeof(depth));
	labelmapSize[0] = height;
	labelmapSize[1] = width;
	labelmapSize[2] = depth;

	std::cout << width << " " << height << " " << depth <<std::endl;

	double temp;
	int *labelmap = new int[width * height * depth];
	for(int i = 0; i < width * height * depth; i++){
		infile.read((char *) &temp, sizeof(temp));
		labelmap[i] = temp;
	}

	int32_t tempInt;
	infile.read((char *) &tempInt, sizeof(tempInt));
	infile.read((char *) &tempInt, sizeof(tempInt));

	for(int i = 0; i < 3; i++){
		infile.read((char *) &temp, sizeof(temp));
		offset[i] = temp;
	}

	infile.read((char *) &tempInt, sizeof(tempInt));
	infile.read((char *) &tempInt, sizeof(tempInt));

	for(int i = 0; i < 3; i++){
		infile.read((char *) &temp, sizeof(temp));
		pixelDimensions[i] = temp;
	}

	return labelmap;
}

int testOptimizer(){
	int nbGPUs = 2;
	Optimizer *lib = new ADMMGscon(nbGPUs);

	std::string beamletPointsInPatientCoordsFile = DATAFOLDER + "beamletPointsInPatientCoords";
	std::string perpPlaneVectorsInPatientCoordsFile = DATAFOLDER + "perpPlaneVectorsInPatientCoords";
	std::string sourcePositionsInPatientCoordsFile = DATAFOLDER + "sourcePositionsInPatientCoords";
	std::string kernelFile = DATAFOLDER + "kernel.bin";
	std::string labelmapFile = DATAFOLDER + "labelmap.bin";
	std::string xSolFile = DATAFOLDER + "xsol.bin";

	int beamletPointsInPatientCoords_Cols, beamletPointsInPatientCoords_Rows;
	SOURCE_PRECISION** beamletPointsInPatientCoords = loadBinary(beamletPointsInPatientCoordsFile, beamletPointsInPatientCoords_Cols, beamletPointsInPatientCoords_Rows);

	int perpPlaneVectorsInPatientCoords_Cols, perpPlaneVectorsInPatientCoords_Rows;
	SOURCE_PRECISION** perpPlaneVectorsInPatientCoords = loadBinary(perpPlaneVectorsInPatientCoordsFile, perpPlaneVectorsInPatientCoords_Cols, perpPlaneVectorsInPatientCoords_Rows);

	int sourcePositionsInPatientCoords_Cols, sourcePositionsInPatientCoords_Rows;
	SOURCE_PRECISION** sourcePositionsInPatientCoords = loadBinary(sourcePositionsInPatientCoordsFile, sourcePositionsInPatientCoords_Cols, sourcePositionsInPatientCoords_Rows);

	int kernel_Cols, kernel_Rows;
	SOURCE_PRECISION** kernel = loadBinary(kernelFile, kernel_Cols, kernel_Rows);

	SOURCE_PRECISION* labelMapeOffset = new SOURCE_PRECISION[3];
	SOURCE_PRECISION* labelMapePixelDimensions = new SOURCE_PRECISION[3];
	int* labelMapeDimensions = new int[3];
	int* labelMap = readLabelMap(labelmapFile, labelMapeDimensions, labelMapeOffset, labelMapePixelDimensions);

	AMatrixParameters matrixA;
	matrixA.setBeamletPointsInPatientCoords(beamletPointsInPatientCoords, beamletPointsInPatientCoords_Rows, beamletPointsInPatientCoords_Cols);
	matrixA.setPerpendicularPlaneVectors(perpPlaneVectorsInPatientCoords, perpPlaneVectorsInPatientCoords_Rows, perpPlaneVectorsInPatientCoords_Cols);
	matrixA.setSourcePointsInPatientCoords(sourcePositionsInPatientCoords, sourcePositionsInPatientCoords_Rows, sourcePositionsInPatientCoords_Cols);
	matrixA.setKernel(kernel, kernel_Rows, kernel_Cols);
	matrixA.setLabelmap(labelMapeOffset, labelMapePixelDimensions, labelMapeDimensions, labelMap);

	lib->setMatrixAParameters(matrixA);

	lib->generateA();

	OptimizationParameters optimization;
	optimization.setBeamCount(90);
	optimization.setBeamWidth(20);
	optimization.setBeamHeight(20);

	optimization.setMaxIterationsStage1(3000);
	optimization.setMaxIterationsStage2(15000);
	optimization.setRelativeErrorStage1(0.1);
	optimization.setRelativeErrorStage2(0.02);
	optimization.setGammaStage1(1.0);
	optimization.setGammaStage2(1.0);

	const int regionTypes = 7;


	//New example
	float regionUpperBounds[regionTypes] = {74.0f, 82, 70.2f, 70.2, 74.0f, 74.0f, 45.0f}; //d1 - d7
	float regionLowerBounds[regionTypes] = {-1,    74, -1,    -1,   70.2f, 70.2f, -1}; //d1 - d7
	int regionIDs[regionTypes] =           {2,      5,  3,     4,       6,     7,  8}; //2-8 (as1 = find(Y==2); %Transition region)
	
	
	//New example, one lower bound
	/*float regionUpperBounds[regionTypes] = {78.0f, 70.2, 70.2f,      82, 70.2f, 70.2f, 45.0f}; //d1 - d7
	float regionLowerBounds[regionTypes] = {-1,        -1, -1,         78,    -1, -1, -1}; //d1 - d7
	int regionIDs[regionTypes] =           {2,          3,  4,     5,       6,     7,  8}; //2-8 (as1 = find(Y==2); %Transition region)	*/
	
	/*Test
	float regionUpperBounds[regionTypes] = {74.0f, 70.2f, 70.2f, 82.0f, 74.0f, 74.0f, 45.0f}; //d1 - d7
	float regionLowerBounds[regionTypes] = {-1,   -1,    -1,     74.0f, 70.2f, 70.2f, -1}; //d1 - d7
	int regionIDs[regionTypes] =           {2,     5,     4,         3,     6,     7,  8}; //2-8 (as1 = find(Y==2); %Transition region)*/

	//Classic
	/*float regionUpperBounds[regionTypes] = {74.0f, 70.2f, 70.2f, 82.0f, 74.0f, 74.0f, 45.0f}; //d1 - d7
	float regionLowerBounds[regionTypes] = {-1, -1, -1, 74.0f, 70.2f, 70.2f, -1}; //d1 - d7
	
	int regionIDs[regionTypes] = {2,3,4,5,6,7,8}; //2-8 (as1 = find(Y==2); %Transition region)*/

	// create regions
	optimization.setRegions(regionTypes,regionUpperBounds,regionLowerBounds, regionIDs);

	lib->setOptimizationParameters(optimization);

	lib->optimize();

	lib->generateDosemap();

	int nnz = 0;
	const int resultSize = lib->getDosemapSize();

	std::cout << "Check " << resultSize << " positions" << std::endl;
	for(int i = 0;i < resultSize; i++){
		if(lib->getDosemap()[i] > 0){
			nnz++;
		}
	}

	const int correctNNZ = 344316;
	if(correctNNZ != nnz){
		std::cout << "***** DOSEMAP IS WRONG! *****" << std::endl;
	}

	std::cout << "Solution dosemap has " << nnz << " nnz" << std::endl;
	std::cout << "Should be " << correctNNZ << std::endl;

	delete lib;

	return 0;
}

/**
 * This class tests the dummy implementation of the optimizer
 */
int main()
{
	testOptimizer();
}
